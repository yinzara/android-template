package com.example.app.dao;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.SqlUtils;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import com.example.app.entity.SecureText;

import com.example.app.entity.Attachment;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table ATTACHMENT.
*/
public class AttachmentDao extends AbstractDao<Attachment, Long> {

    public static final String TABLENAME = "ATTACHMENT";

    /**
     * Properties of entity Attachment.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property<Long> Id = new Property<>(0, long.class, "id", true, "_id");
        public final static Property<String> Filename = new Property<>(1, String.class, "filename", false, "FILENAME");
        public final static Property<Long> SecureTextOwnerId = new Property<>(2, long.class, "secureTextOwnerId", false, "SECURE_TEXT_OWNER_ID");
    };

    private DaoSession daoSession;

    private Query<Attachment> secureText_AttachmentsQuery;

    public AttachmentDao(DaoConfig config) {
        super(config);
    }
    
    public AttachmentDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'ATTACHMENT' (" + //
                "'_id' INTEGER PRIMARY KEY NOT NULL ," + // 0: id
                "'FILENAME' TEXT NOT NULL ," + // 1: filename
                "'SECURE_TEXT_OWNER_ID' INTEGER NOT NULL );"); // 2: secureTextOwnerId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'ATTACHMENT'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Attachment entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getId());
        stmt.bindString(2, entity.getFilename());
        stmt.bindLong(3, entity.getSecureTextOwnerId());
    }

    @Override
    protected void attachEntity(Attachment entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Attachment readEntity(Cursor cursor, int offset) {
        Attachment entity = new Attachment( //
            cursor.getLong(offset + 0), // id
            cursor.getString(offset + 1), // filename
            cursor.getLong(offset + 2) // secureTextOwnerId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Attachment entity, int offset) {
        entity.setId(cursor.getLong(offset + 0));
        entity.setFilename(cursor.getString(offset + 1));
        entity.setSecureTextOwnerId(cursor.getLong(offset + 2));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Attachment entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Attachment entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "attachments" to-many relationship of SecureText. */
    public List<Attachment> _querySecureText_Attachments(long secureTextOwnerId) {
        synchronized (this) {
            if (secureText_AttachmentsQuery == null) {
                QueryBuilder<Attachment> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.SecureTextOwnerId.eq(null));
                secureText_AttachmentsQuery = queryBuilder.build();
            }
        }
        Query<Attachment> query = secureText_AttachmentsQuery.forCurrentThread();
        query.setParameter(0, secureTextOwnerId);
        return query.list();
    }

    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getSecureTextDao().getAllColumns());
            builder.append(" FROM ATTACHMENT T");
            builder.append(" LEFT JOIN SECURE_TEXT T0 ON T.'SECURE_TEXT_OWNER_ID'=T0.'_id'");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected Attachment loadCurrentDeep(Cursor cursor, boolean lock) {
        Attachment entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        SecureText secureText = loadCurrentOther(daoSession.getSecureTextDao(), cursor, offset);
         if(secureText != null) {
            entity.setSecureText(secureText);
        }

        return entity;    
    }

    public Attachment loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<Attachment> loadAllDeepFromCursor(Cursor cursor, int offset, int maxResults) {
        int count = cursor.getCount();

        if (count < offset) {
            return new ArrayList<Attachment>(0);
        }
        count = count > maxResults?maxResults:count;
        List<Attachment> list = new ArrayList<Attachment>(count);
        
        if (cursor.moveToPosition(0)) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                for (int i = 0; i < count; i++) {
                    list.add(loadCurrentDeep(cursor, false));
                    cursor.moveToNext();
                }
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }

    protected List<Attachment> loadDeepAllAndCloseCursor(Cursor cursor) {
        return loadDeepAllAndCloseCursor(cursor, 0, Integer.MAX_VALUE);
    }
    
    protected List<Attachment> loadDeepAllAndCloseCursor(Cursor cursor, int offset, int maxResult) {
        try {
            return loadAllDeepFromCursor(cursor, offset, maxResult);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<Attachment> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor, 0, Integer.MAX_VALUE);
    }

    /** A raw-style query where you can pass any WHERE clause and arguments. */
        public List<Attachment> queryDeep(String where, int offset, int maxResults) {
            Cursor cursor = db.rawQuery(getSelectDeep() + where, null, offset + maxResults, offset + maxResults);
            return loadDeepAllAndCloseCursor(cursor, offset, maxResults);
        }
 
}
