package com.example.app.dao;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.SqlUtils;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import com.example.app.entity.OrganizationalGroup;

import com.example.app.entity.OrganizationalGroup;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table ORGANIZATIONAL_GROUP.
*/
public class OrganizationalGroupDao extends AbstractDao<OrganizationalGroup, Long> {

    public static final String TABLENAME = "ORGANIZATIONAL_GROUP";

    /**
     * Properties of entity OrganizationalGroup.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property<Long> Id = new Property<>(0, long.class, "id", true, "_id");
        public final static Property<String> Name = new Property<>(1, String.class, "name", false, "NAME");
        public final static Property<Long> ParentId = new Property<>(2, Long.class, "parentId", false, "PARENT_ID");
    };

    private DaoSession daoSession;

    private Query<OrganizationalGroup> organizationalGroup_ChildrenQuery;

    public OrganizationalGroupDao(DaoConfig config) {
        super(config);
    }
    
    public OrganizationalGroupDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'ORGANIZATIONAL_GROUP' (" + //
                "'_id' INTEGER PRIMARY KEY NOT NULL ," + // 0: id
                "'NAME' TEXT," + // 1: name
                "'PARENT_ID' INTEGER);"); // 2: parentId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'ORGANIZATIONAL_GROUP'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, OrganizationalGroup entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.getId());
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(2, name);
        }
 
        Long parentId = entity.getParentId();
        if (parentId != null) {
            stmt.bindLong(3, parentId);
        }
    }

    @Override
    protected void attachEntity(OrganizationalGroup entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public OrganizationalGroup readEntity(Cursor cursor, int offset) {
        OrganizationalGroup entity = new OrganizationalGroup( //
            cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // name
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2) // parentId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, OrganizationalGroup entity, int offset) {
        entity.setId(cursor.getLong(offset + 0));
        entity.setName(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setParentId(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(OrganizationalGroup entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(OrganizationalGroup entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "children" to-many relationship of OrganizationalGroup. */
    public List<OrganizationalGroup> _queryOrganizationalGroup_Children(Long parentId) {
        synchronized (this) {
            if (organizationalGroup_ChildrenQuery == null) {
                QueryBuilder<OrganizationalGroup> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.ParentId.eq(null));
                organizationalGroup_ChildrenQuery = queryBuilder.build();
            }
        }
        Query<OrganizationalGroup> query = organizationalGroup_ChildrenQuery.forCurrentThread();
        query.setParameter(0, parentId);
        return query.list();
    }

    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getOrganizationalGroupDao().getAllColumns());
            builder.append(" FROM ORGANIZATIONAL_GROUP T");
            builder.append(" LEFT JOIN ORGANIZATIONAL_GROUP T0 ON T.'PARENT_ID'=T0.'_id'");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected OrganizationalGroup loadCurrentDeep(Cursor cursor, boolean lock) {
        OrganizationalGroup entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        OrganizationalGroup parent = loadCurrentOther(daoSession.getOrganizationalGroupDao(), cursor, offset);
        entity.setParent(parent);

        return entity;    
    }

    public OrganizationalGroup loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<OrganizationalGroup> loadAllDeepFromCursor(Cursor cursor, int offset, int maxResults) {
        int count = cursor.getCount();

        if (count < offset) {
            return new ArrayList<OrganizationalGroup>(0);
        }
        count = count > maxResults?maxResults:count;
        List<OrganizationalGroup> list = new ArrayList<OrganizationalGroup>(count);
        
        if (cursor.moveToPosition(0)) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                for (int i = 0; i < count; i++) {
                    list.add(loadCurrentDeep(cursor, false));
                    cursor.moveToNext();
                }
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }

    protected List<OrganizationalGroup> loadDeepAllAndCloseCursor(Cursor cursor) {
        return loadDeepAllAndCloseCursor(cursor, 0, Integer.MAX_VALUE);
    }
    
    protected List<OrganizationalGroup> loadDeepAllAndCloseCursor(Cursor cursor, int offset, int maxResult) {
        try {
            return loadAllDeepFromCursor(cursor, offset, maxResult);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<OrganizationalGroup> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor, 0, Integer.MAX_VALUE);
    }

    /** A raw-style query where you can pass any WHERE clause and arguments. */
        public List<OrganizationalGroup> queryDeep(String where, int offset, int maxResults) {
            Cursor cursor = db.rawQuery(getSelectDeep() + where, null, offset + maxResults, offset + maxResults);
            return loadDeepAllAndCloseCursor(cursor, offset, maxResults);
        }
 
}
