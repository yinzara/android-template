package com.example.app.entity.view.item;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.example.app.entity.Conversation;
import com.example.app.view.base.EntityBindable;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

// KEEP INCLUDES - put your custom includes here
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import com.example.app.entity.SecureText;
import android.widget.TextView;
import org.androidannotations.annotations.ViewById;
// KEEP INCLUDES END

/**
 * Created by yinzara on 8/13/15.
 */
@EViewGroup
public class ConversationListItemView extends LinearLayout implements EntityBindable<Conversation> {

    protected Conversation conversation;

    //////////
    ///Inject views by Id
    ///////
    // KEEP FIELDS - put your custom fields here
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.fullDateTime();

    @ViewById
    TextView messageText;

    @ViewById
    TextView toText;

    @ViewById
    TextView fromText;

    @ViewById
    TextView sentText;
    // KEEP FIELDS END

    public ConversationListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void bind(Conversation entity) {
        this.conversation = entity;
        ////////Bind entity to views
        // KEEP BODY - put your custom methods here
        //I can only access the last message field inside the bind method because I used queryDeep inside the adapter
        final SecureText txt = entity.getLastMessage();
        messageText.setText(txt.getMessage());
        toText.setText(txt.getTo());
        fromText.setText(txt.getFrom());
        sentText.setText(FORMATTER.print(txt.getSent().getTime()));
        // KEEP BODY END
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
