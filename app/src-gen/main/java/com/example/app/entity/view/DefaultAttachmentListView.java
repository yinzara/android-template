/*
 * Copyright 2013 Square Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.app.entity.view;

import android.content.Context;
import android.util.AttributeSet;

import com.example.app.R;
import com.example.app.view.base.adapter.EntityAdapter;
import com.example.app.entity.view.adapter.AttachmentListAdapter;
import com.example.app.entity.Attachment;
import com.example.app.entity.view.base.BaseAttachmentListView;
import com.example.app.dagger.component.ViewComponent;

import org.androidannotations.annotations.EViewGroup;

import javax.inject.Inject;

@EViewGroup
public class DefaultAttachmentListView extends BaseAttachmentListView {

    @Inject
    AttachmentListAdapter adapter;

    public DefaultAttachmentListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected EntityAdapter<Attachment,?> getEntityAdapter() {
        return adapter;
    }

    @Override
    protected void doInject(ViewComponent injector) {
        injector.inject(this);
    }
}
