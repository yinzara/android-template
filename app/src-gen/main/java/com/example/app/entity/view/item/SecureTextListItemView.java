package com.example.app.entity.view.item;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.example.app.entity.SecureText;
import com.example.app.view.base.EntityBindable;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

// KEEP INCLUDES - put your custom includes here
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import android.widget.TextView;
import org.androidannotations.annotations.ViewById;
// KEEP INCLUDES END

/**
 * Created by yinzara on 8/13/15.
 */
@EViewGroup
public class SecureTextListItemView extends LinearLayout implements EntityBindable<SecureText> {

    protected SecureText secureText;

    //////////
    ///Inject views by Id
    ///////
    // KEEP FIELDS - put your custom fields here
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.fullDateTime();

    @ViewById
    TextView messageText;

    @ViewById
    TextView toText;

    @ViewById
    TextView fromText;

    @ViewById
    TextView sentText;
    // KEEP FIELDS END

    public SecureTextListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void bind(SecureText entity) {
        this.secureText = entity;
        ////////Bind entity to views
        // KEEP BODY - put your custom methods here
        messageText.setText(entity.getMessage());
        toText.setText(entity.getTo());
        fromText.setText(entity.getFrom());
        sentText.setText(FORMATTER.print(entity.getSent().getTime()));
        // KEEP BODY END
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
