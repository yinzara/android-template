package com.example.app.entity;

import java.util.List;
import com.example.app.dao.DaoSession;
import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.DaoException;
import de.greenrobot.dao.HasId;


import android.os.Parcel;
import android.os.Parcelable;
import com.example.app.dao.ConversationDao;
import com.example.app.dao.SecureTextDao;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table CONVERSATION.
 */
public class Conversation implements Parcelable, HasId {

    private long id;
    private boolean deleted;
    private java.util.Date lastActivity;
    private Long lastMessageId;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient ConversationDao myDao;

    private SecureText lastMessage;
    private Long lastMessage__resolvedKey;

    private List<SecureText> messages;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public Conversation() {
    }

    public Conversation(long id) {
        this.id = id;
    }

    public Conversation(long id, boolean deleted, java.util.Date lastActivity, Long lastMessageId) {
        this.id = id;
        this.deleted = deleted;
        this.lastActivity = lastActivity;
        this.lastMessageId = lastMessageId;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getConversationDao() : null;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public java.util.Date getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(java.util.Date lastActivity) {
        this.lastActivity = lastActivity;
    }

    public Long getLastMessageId() {
        return lastMessageId;
    }

    public void setLastMessageId(Long lastMessageId) {
        this.lastMessageId = lastMessageId;
    }

    /** To-one relationship, resolved on first access. */
    public SecureText getLastMessage() {
        Long __key = this.lastMessageId;
        if (lastMessage__resolvedKey == null || !lastMessage__resolvedKey.equals(__key)) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            SecureTextDao targetDao = daoSession.getSecureTextDao();
            SecureText lastMessageNew = targetDao.load(__key);
            synchronized (this) {
                lastMessage = lastMessageNew;
            	lastMessage__resolvedKey = __key;
            }
        }
        return lastMessage;
    }

    public void setLastMessage(SecureText lastMessage) {
        synchronized (this) {
            this.lastMessage = lastMessage;
            lastMessageId = lastMessage == null ? null : lastMessage.getId();
            lastMessage__resolvedKey = lastMessageId;
        }
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<SecureText> getMessages() {
        if (messages == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            SecureTextDao targetDao = daoSession.getSecureTextDao();
            List<SecureText> messagesNew = targetDao._queryConversation_Messages(id);
            synchronized (this) {
                if(messages == null) {
                    messages = messagesNew;
                }
            }
        }
        return messages;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetMessages() {
        messages = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

    @Override
    public void writeToParcel(Parcel _dst, int _flgs) {
        //Properties
        _dst.writeLong(id);
        _dst.writeInt(deleted?1:-1);
        if (lastActivity != null) {
            _dst.writeInt(1);
            _dst.writeLong(lastActivity.getTime());
        } else {
            _dst.writeInt(0);
        }
        if (lastMessageId != null) {
            _dst.writeInt(1);
            _dst.writeLong(lastMessageId);
        } else {
            _dst.writeInt(0);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @SuppressWarnings({"rawtypes","unchecked"})
    public static final Parcelable.Creator<Conversation> CREATOR = new Parcelable.Creator<Conversation>() {
        @Override
        public Conversation createFromParcel(Parcel source) {
            final Conversation entity = new Conversation();
            //Properties
            entity.id = source.readLong();
            entity.deleted = source.readInt() > 0;
            if (source.readInt() == 1) {
                entity.lastActivity = new java.util.Date(source.readLong());
            }
            if (source.readInt() == 1) {
                entity.lastMessageId = (Long)source.readLong();
            }


            return entity;
        }

        @Override
        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };


    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
