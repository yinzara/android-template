package com.example.app.entity.view.item;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.example.app.entity.OrganizationalGroup;
import com.example.app.view.base.EntityBindable;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Created by yinzara on 8/13/15.
 */
@EViewGroup
public class OrganizationalGroupListItemView extends LinearLayout implements EntityBindable<OrganizationalGroup> {

    protected OrganizationalGroup organizationalGroup;

    //////////
    ///Inject views by Id
    ///////
    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public OrganizationalGroupListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void bind(OrganizationalGroup entity) {
        this.organizationalGroup = entity;
        ////////Bind entity to views
        // KEEP BODY - put your custom methods here
        // KEEP BODY END
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
