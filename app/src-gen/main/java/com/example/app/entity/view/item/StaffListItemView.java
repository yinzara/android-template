package com.example.app.entity.view.item;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.example.app.entity.Staff;
import com.example.app.view.base.EntityBindable;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Created by yinzara on 8/13/15.
 */
@EViewGroup
public class StaffListItemView extends LinearLayout implements EntityBindable<Staff> {

    protected Staff staff;

    //////////
    ///Inject views by Id
    ///////
    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public StaffListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void bind(Staff entity) {
        this.staff = entity;
        ////////Bind entity to views
        // KEEP BODY - put your custom methods here
        // KEEP BODY END
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
