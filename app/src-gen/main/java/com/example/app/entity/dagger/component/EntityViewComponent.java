package com.example.app.entity.dagger.component;

import com.example.app.dagger.ViewScoped;
import dagger.Subcomponent;
import com.example.app.entity.view.DefaultOrganizationalGroupListView;
import com.example.app.entity.view.DefaultStaffListView;
import com.example.app.entity.view.DefaultConversationListView;
import com.example.app.entity.view.DefaultSecureTextListView;
import com.example.app.entity.view.DefaultAttachmentListView;
import com.example.app.entity.view.DefaultVoicemailListView;

@ViewScoped
@Subcomponent
public interface EntityViewComponent {

   void inject(DefaultOrganizationalGroupListView view);
   void inject(DefaultStaffListView view);
   void inject(DefaultConversationListView view);
   void inject(DefaultSecureTextListView view);
   void inject(DefaultAttachmentListView view);
   void inject(DefaultVoicemailListView view);

}
