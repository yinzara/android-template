# Doesn't require special rules
# * AndroidAnnotation
# * commmons-io
# * Dagger 2
# * Flow

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}
-dontwarn com.bumptech.glide.manager.SupportRequestManagerFragment

# GreenDAO
-keepclassmembers class * extends de.greenrobot.dao.AbstractDao {
    public static java.lang.String TABLENAME;
}
-keep class **$Properties

# Jackson 2
-keepattributes EnclosingMethod,Signature
-keepnames class com.fasterxml.jackson.** { *; }
-dontwarn com.fasterxml.jackson.databind.**

# Joda Time 2.3
-dontwarn org.joda.convert.**
-dontwarn org.joda.time.**
-keep class org.joda.time.** { *; }
-keep interface org.joda.time.** { *; }

# LeakCanary <1.3 (if you upgrade, remove this)
-keep class org.eclipse.mat.** { *; }
-keep class com.squareup.leakcanary.** { *; }

# OkHttp
-dontwarn rx.**
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.internal.huc.HttpsURLConnectionImpl

# Okio
-dontwarn okio.**

# Otto
-keepattributes *Annotation*
-keepclassmembers class ** {
    @com.squareup.otto.Subscribe public *;
    @com.squareup.otto.Produce public *;
}

# SQLCipher
-keep class net.sqlcipher.** { *; }
-keep class net.sqlcipher.database.* { *; }

# Spring Templates
-dontwarn org.springframework.http.converter.**

# VST DTO objects (since they're used by Jackson)
-keep class com.example.app.dto.** { *; }
-keep class com.codeware.dto.** { *; }
-keepnames class com.codeware.dto.** { *; }
-keepnames class com.example.app.dto.** { *; }

-dontwarn com.codeware.**
-dontwarn com.twilio.**
-dontwarn com.example.app.daogenerator.Generate
-dontwarn java.lang.invoke.**
