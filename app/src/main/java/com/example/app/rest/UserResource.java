package com.example.app.rest;

import com.example.app.rest.base.DebugInterceptor;
import com.example.app.rest.base.JacksonMessageConverter;
import com.example.app.rest.base.RequestFactory;
import com.example.app.rest.dto.LoginResponse;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientRootUrl;

/**
 * Created by yinzara on 8/14/15.
 */
@Rest(converters = JacksonMessageConverter.class, requestFactory = RequestFactory.class, interceptors = DebugInterceptor.class)
@Accept("application/json")
public interface UserResource extends RestClientRootUrl {

    @Get("/security/authenticate?username={username}&pin={pin}")
    LoginResponse login(final String username, final String pin);

}
