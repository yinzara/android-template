package com.example.app.rest.dto;

import com.codeware.dto.security.UserDTO;
import com.fasterxml.jackson.annotation.JsonFormat;

import org.joda.time.DateTime;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by yinzara on 8/14/15.
 */
public class LoginResponse extends BaseResponse {


    @Getter
    @Setter
    private String token, voiceToken, networkNumber;

    @Getter
    @Setter
    private UserDTO user;

    @Getter
    @Setter
    @JsonFormat(timezone = "UTC")
    private DateTime tokenExpiration;
}
