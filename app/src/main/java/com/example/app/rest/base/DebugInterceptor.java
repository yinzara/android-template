package com.example.app.rest.base;

import android.util.Log;

import com.example.app.BuildConfig;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * Created by yinzara on 8/19/15.
 */
public class DebugInterceptor implements ClientHttpRequestInterceptor {

    private static final String TAG = "REST";

    @Override
    public ClientHttpResponse intercept(HttpRequest req, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "HTTP " + req.getMethod() + " to " + req.getURI().toString() + " with content type " + req.getHeaders().getContentType());
        }
        final ClientHttpResponse response = execution.execute(req, body);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Resp to " + req.getMethod() + " " + req.getURI() + " complete with code " + response.getRawStatusCode());
        }
        return response;
    }
}
