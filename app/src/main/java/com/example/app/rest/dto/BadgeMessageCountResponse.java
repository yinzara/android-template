package com.example.app.rest.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by yinzara on 8/15/15.
 */
public class BadgeMessageCountResponse extends BaseResponse {

    @Getter
    @Setter
    private Integer badge;
}
