package com.example.app.rest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import org.cometd.common.HashMapMessage;

/**
 * Created by yinzara on 8/20/15.
 */
public class CometDMessage extends HashMapMessage {

    private CometDMessageData data;

    @Override
    @JsonDeserialize(as = CometDMessageData.class)
    public void setData(Object data) {
        if (!(data instanceof CometDMessage)) {
            throw new IllegalArgumentException("Data must be a CometDMessage");
        }
        this.data = (CometDMessageData)data;
    }

    @Override
    public CometDMessageData getData() {
        return data;
    }
}
