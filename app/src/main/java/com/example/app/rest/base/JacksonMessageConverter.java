package com.example.app.rest.base;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.example.app.BuildConfig;
import com.example.app.VSTApplication;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.input.TeeInputStream;
import org.apache.commons.io.output.TeeOutputStream;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

import javax.inject.Inject;

/**
 * Special version of MappingJackson2HttpMessageConverter<br/>
 * New features:
 * <ul>
 * <li>Uses ObjectMapper from Dagger 2 context</li>
 * <li>In the case of a DEBUG build, prints the formatted JSON to the log at debug level</li>
 * </ul>
 * <p>
 * Created by yinzara on 8/14/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class JacksonMessageConverter extends MappingJackson2HttpMessageConverter {

    private static final String TAG = "REST";

    @RootContext
    Context context;

    @Inject
    ObjectMapper objectMapper;

    @AfterInject
    void afterInject() {
        VSTApplication.injector(context).inject(this);
        setObjectMapper(objectMapper);
    }

    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType) {
        return canRead(mediaType);
    }

    @Override
    public boolean canRead(Type type, Class<?> contextClass, MediaType mediaType) {
        return canRead(mediaType);
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return canWrite(mediaType);
    }

    @Override
    protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage)
            throws IOException, HttpMessageNotReadableException {

        JavaType javaType = getJavaType(clazz, null);
        return readJavaType(javaType, inputMessage);
    }

    public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage)
            throws IOException, HttpMessageNotReadableException {

        JavaType javaType = getJavaType(type, contextClass);
        return readJavaType(javaType, inputMessage);
    }

    private Object readJavaType(JavaType javaType, HttpInputMessage inputMessage) {
        ByteArrayOutputStream baos = null;
        try {
            if (BuildConfig.DEBUG) {
                baos = new ByteArrayOutputStream();
                final Object value = this.objectMapper.readValue(new TeeInputStream(inputMessage.getBody(), baos, false), javaType);
                writeJsonException(null, baos);
                return value;
            } else {
                baos = null;
                return this.objectMapper.readValue(inputMessage.getBody(), javaType);
            }
        } catch (IOException ex) {
            writeJsonException(ex, baos);
            throw new HttpMessageNotReadableException("Could not read JSON: " + ex.getMessage(), ex);
        }
    }

    private void writeJsonException(IOException ex, ByteArrayOutputStream baos) {
        if (baos != null && BuildConfig.DEBUG) {
            try {
                String json = baos.toString("UTF-8");
                try {
                    json = new JSONObject(json).toString(4);
                } catch (JSONException exp) {
                    //Do nothing as we'll just print the string as is
                }

                if (ex != null) {
                    for (int i = 0; i * 500 < json.length();i++) {
                        final int startIndex = i * 500;
                        int endIndex = (i + 1) * 500;
                        endIndex = Math.min(json.length(), endIndex);
                        Log.d(TAG, json.substring(startIndex, endIndex), ex);
                    }
                } else {
                    for (int i = 0; i * 500 < json.length();i++) {
                        final int startIndex = i * 500;
                        int endIndex = (i + 1) * 500;
                        endIndex = Math.min(json.length(), endIndex);
                        Log.d(TAG, json.substring(startIndex, endIndex));
                    }
                }
            } catch (UnsupportedEncodingException exp) {
                //Do nothing, useless and can't happen
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void writeInternal(Object object, HttpOutputMessage outputMessage)
            throws IOException, HttpMessageNotWritableException {

        final JsonEncoding encoding = getJsonEncoding(outputMessage.getHeaders().getContentType());
        final JsonGenerator jsonGenerator;

        ByteArrayOutputStream baos = null;
        try {
            if (BuildConfig.DEBUG) {
                baos = new ByteArrayOutputStream();
                jsonGenerator =
                        this.objectMapper.getJsonFactory().createJsonGenerator(new TeeOutputStream(outputMessage.getBody(), baos), encoding);
                baos.flush();
            } else {
                baos = null;
                jsonGenerator =
                        this.objectMapper.getJsonFactory().createJsonGenerator(outputMessage.getBody(), encoding);
            }
            // A workaround for JsonGenerators not applying serialization features
            // https://github.com/FasterXML/jackson-databind/issues/12
            if (this.objectMapper.isEnabled(SerializationFeature.INDENT_OUTPUT)) {
                jsonGenerator.useDefaultPrettyPrinter();
            }

            try {
                this.objectMapper.writeValue(jsonGenerator, object);
            } catch (JsonProcessingException ex) {
                throw new HttpMessageNotWritableException("Could not write JSON: " + ex.getMessage(), ex);
            }
            if (baos != null) {
                writeJsonException(null, baos);
            }
        } finally {
            if (baos != null) {
                baos.close();
            }
        }
    }
}
