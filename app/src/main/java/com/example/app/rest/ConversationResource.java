package com.example.app.rest;

import com.example.app.rest.base.DebugInterceptor;
import com.example.app.rest.base.JacksonMessageConverter;
import com.example.app.rest.base.RequestFactory;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientRootUrl;

/**
 * Created by yinzara on 8/15/15.
 */
@Rest(converters = JacksonMessageConverter.class, requestFactory = RequestFactory.class, interceptors = DebugInterceptor.class)
@Accept("application/json")
public interface ConversationResource extends RestClientRootUrl {
}
