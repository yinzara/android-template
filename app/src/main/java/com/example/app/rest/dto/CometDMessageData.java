package com.example.app.rest.dto;

import com.codeware.dto.message.VoicemailNotificationDTO;
import com.codeware.dto.securetext.SecureTextDTO;
import com.codeware.dto.staff.StaffDTO;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.example.app.entity.SecureText;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by yinzara on 8/20/15.
 */
@NoArgsConstructor
@AllArgsConstructor
public class CometDMessageData {

    public enum MessageAction {
        VOICEMAIL(null),
        NEW(SecureTextDTO.SecureTextAction.NEW),
        UPDATE(SecureTextDTO.SecureTextAction.UPDATE),
        NOTIFICATION(SecureTextDTO.SecureTextAction.NOTIFICATION),
        DELIVERY(SecureTextDTO.SecureTextAction.DELIVERY),
        READ(SecureTextDTO.SecureTextAction.READ),
        DELETE(SecureTextDTO.SecureTextAction.DELETE),
        SCHEDULED(SecureTextDTO.SecureTextAction.SCHEDULED);

        private static Map<String, MessageAction> namesMap;

        private final SecureTextDTO.SecureTextAction action;

        private MessageAction(SecureTextDTO.SecureTextAction action) {
            this.action = action;
        }

        @JsonCreator
        public static MessageAction forValue(String value) {
            if (namesMap == null) {
                final Map<String, MessageAction> temp = new HashMap<String, MessageAction>();
                for (final MessageAction val : values()) {
                    temp.put(val.name(), val);
                }
                namesMap = temp;
            }
            return namesMap.get(value.toUpperCase());
        }

        @JsonValue
        public String toValue() {
            return name();
        }

        public SecureTextDTO.SecureTextAction getAction() {
            return action;
        }
    }

    @Getter
    @Setter
    private VoicemailNotificationDTO voicemail;

    @Getter
    @Setter
    private SecureTextDTO text;

    @Getter
    @Setter
    private StaffDTO staff;

    @Getter
    @Setter
    private String source;

    @Getter
    @Setter
    private String scope;

    @Getter
    @Setter
    private MessageAction action;
}
