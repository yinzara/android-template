package com.example.app.rest.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by yinzara on 8/14/15.
 */
public abstract class BaseResponse {


    @Getter
    @Setter
    private String status, error;


    public boolean isSuccess() {
        return "ok".equalsIgnoreCase(status);
    }
}
