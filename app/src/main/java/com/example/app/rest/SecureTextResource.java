package com.example.app.rest;

import com.codeware.dto.securetext.SecureTextDTO;
import com.codeware.dto.securetext.SecureTextListResultDTO;
import com.example.app.rest.base.DebugInterceptor;
import com.example.app.rest.base.JacksonMessageConverter;
import com.example.app.rest.base.RequestFactory;
import com.example.app.rest.dto.BadgeMessageCountResponse;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Delete;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientRootUrl;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by yinzara on 8/15/15.
 */
@Rest(converters = JacksonMessageConverter.class, requestFactory = RequestFactory.class, interceptors = DebugInterceptor.class)
@Accept("application/json")
public interface SecureTextResource extends RestClientRootUrl {

    @Get("/secure-text/nc")
    BadgeMessageCountResponse getUnreadCount();

    @Delete("/st/{secureTextOwnerId}/delete")
    void deleteSecureText(long secureTextOwnerId);

    @Get("/v3/secure-texts.json?sdt={newerThan}&maxResults={count}&firstResult={offset}")
    SecureTextListResultDTO getMostRecentSecureTexts(final DateTime newerThan, int offset, int count);

    @Post("/st/delivered")
    void postSecureTextsDelivered(final long ... ids);

    @Post("/st/read")
    void postSecureTextsRead(final long ... ids);
}
