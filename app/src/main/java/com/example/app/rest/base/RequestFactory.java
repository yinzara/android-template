package com.example.app.rest.base;

import android.content.Context;
import android.util.Log;

import com.example.app.BuildConfig;
import com.example.app.state.LoginState_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.springframework.http.client.OkHttpRequestFactory;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.inject.Inject;

/**
 * Created by yinzara on 8/14/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class RequestFactory extends OkHttpRequestFactory {

    private static final String TAG = "REST";

    @RootContext
    Context context;

    @Pref
    LoginState_ loginState;

    public RequestFactory() {
        super();
    }

    @Override
    protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
        super.prepareConnection(connection, httpMethod);
        if (loginState.token().exists()) {
            connection.addRequestProperty("x-prana-token", loginState.token().get());
        }
        connection.addRequestProperty("x-prana-version", "3");
    }
}
