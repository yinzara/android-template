/**
 * This package contains interfaces that represent RESTful web services<br/>
 * To use one of these resources, @Inject it into a class that supports Dagger 2 injection<br/>
 * DO NOT inject an interface via the @RestService annotation, it will not have the base URL set correctly<br/>
 * To create a new web service interface:<br/>
 * <ol>
 *     <li>Create a new interface that implements 'RestClientRootUrl'<br/> named 'resourceType'Resource now called 'MyNewResource' for this example</li>
 *     <li>Add the @Rest(converters = JacksonMessageConverter.class, requestFactory = RequestFactory.class, interceptors = DebugInterceptor.class) and @Accept("application/json") annotations to MyNewResource</li>
 *     <li>Add a 'providesMyNewResource' method to WebServiceModule class (don't forget the underscore)
 *     <code>
             @Singleton
             @Provides
             MyNewResource provideMyNewResource() {
                return init(new MyNewResource_(app));
             }
 *     </code>
 *     </li>
 * </ol>
 */
package com.example.app.rest;