package com.example.app.view.securetext.conversation;

import android.content.Context;
import android.database.Cursor;

import com.example.app.dao.SecureTextDao;
import com.example.app.entity.view.adapter.SecureTextCursorAdapter;
import com.example.app.view.Paths;

import javax.inject.Inject;

import flow.path.Path;

/**
 * Created by yinzara on 8/19/15.
 */
public class ConversationAdapter extends SecureTextCursorAdapter {

    @Inject
    public ConversationAdapter(Context context) {
        super(context);
    }


    @Override
    protected Cursor query() {
        Paths.ConversationPath path = Path.get(context);
        return _SecureTextDao.queryBuilder()
                .where(SecureTextDao.Properties.ThreadId.eq(path.getConversationId()))
                .orderDesc(SecureTextDao.Properties.Sent).rawQuery();
    }
}
