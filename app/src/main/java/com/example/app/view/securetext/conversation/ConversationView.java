/*
 * Copyright 2013 Square Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.app.view.securetext.conversation;

import android.content.Context;
import android.util.AttributeSet;

import com.example.app.dagger.component.ViewComponent;
import com.example.app.entity.SecureText;
import com.example.app.entity.view.base.BaseSecureTextListView;
import com.example.app.view.base.adapter.BaseEntityAdapter;
import com.example.app.view.base.adapter.EntityAdapter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EViewGroup;

import javax.inject.Inject;

@EViewGroup
public class ConversationView extends BaseSecureTextListView {

    @Inject
    ConversationAdapter adapter;

    @Bean
    ConversationPresenter presenter;

    public ConversationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void doInject(ViewComponent injector) {
        injector.inject(this);
    }

    @Override
    protected EntityAdapter<SecureText, ?> getEntityAdapter() {
        return adapter;
    }

    @Override
    public ConversationPresenter getPresenter() {
        return presenter;
    }
}
