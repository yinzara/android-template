package com.example.app.view.securetext.conversation;

import android.os.Bundle;

import com.example.app.dagger.component.PresenterComponent;
import com.example.app.view.base.presenter.ScreenPresenter;

import org.androidannotations.annotations.EBean;

/**
 * Created by yinzara on 8/19/15.
 */
@EBean
public class ConversationPresenter extends ScreenPresenter<ConversationView> {

    @Override
    protected void doInject(PresenterComponent component) {
        component.inject(this);
    }


}
