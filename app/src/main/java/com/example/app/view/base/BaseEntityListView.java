package com.example.app.view.base;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;

import com.squareup.otto.Subscribe;

import de.greenrobot.dao.HasId;
import com.example.app.event.EntityEvent;
import com.example.app.event.EntityListEvent;
import com.example.app.view.base.adapter.EntityAdapter;

/**
 * Created by yinzara on 8/17/15.
 */
public abstract class BaseEntityListView<T extends HasId & Parcelable, EventType extends EntityEvent<T>, EventListType extends EntityListEvent<T>> extends BaseListView {

    public BaseEntityListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAdapter(getEntityAdapter());
    }

    protected abstract EntityAdapter<T, ?> getEntityAdapter();

    @Subscribe
    public void onEvent(EventType event) {
        if (getEntityAdapter() != null) {
            getEntityAdapter().onEvent(event, this::onEventUpdateComplete);
        }
    }

    @Subscribe
    public void onEvent(EventListType event) {
        if (getEntityAdapter() != null) {
            getEntityAdapter().onEvent(event, this::onEventUpdateComplete);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public EntityAdapter<T, ?> getAdapter() {
        return (EntityAdapter<T,?>)super.getAdapter();
    }

    protected void onEventUpdateComplete() {

    }
}
