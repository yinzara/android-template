package com.example.app.view.securetext.message;

import com.example.app.R;
import com.example.app.view.base.pathview.Layout;
import com.example.app.view.Paths;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by yinzara on 8/17/15.
 */
@Layout(R.layout.message_view) //
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class MessageScreen extends Paths.ConversationPath {
    @Getter
    @Setter
    private int messageId;

    public MessageScreen(int conversationId, int messageId) {
        super(conversationId);
        this.messageId = messageId;
    }

}
