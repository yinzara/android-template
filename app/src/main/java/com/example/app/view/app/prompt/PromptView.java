package com.example.app.view.app.prompt;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import com.example.app.dagger.component.ViewComponent;
import com.example.app.view.base.BaseLinearLayout;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import flow.Flow;
import flow.History;
import flow.path.Path;

/**
 * Created by yinzara on 8/14/15.
 */
@EViewGroup
public class PromptView extends BaseLinearLayout {

    @ViewById
    TextView messageText;

    @ViewById
    Button okButton;

    @ViewById
    Button cancelButton;

    private PromptModal prompt;

    private Flow flow;

    public PromptView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        flow = Flow.get(this);

        prompt = Path.get(this.getContext());
    }

    @Click
    void cancelButton() {
        replaceFlow(prompt.getCancelState());
    }

    @Click
    void okButton() {
        replaceFlow(prompt.getOkState());
    }

    /**
     * We don't want to allow the user to press back to go to this state so we must replace this state with the next one
     *
     * @param nextState
     */
    private void replaceFlow(final Object nextState) {
        if (nextState == PromptModal.EXIT_STATE) {
            android.os.Process.killProcess(android.os.Process.myPid());
        } else if (nextState == PromptModal.BACK_STATE) {
            flow.goBack();
        } else {
            final History.Builder builder = flow.getHistory().buildUpon();
            builder.pop();
            builder.push(nextState);
            flow.setHistory(builder.build(), Flow.Direction.FORWARD);
        }
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (prompt.getMessageResId() != 0) {
            messageText.setText(prompt.getMessageResId());
        } else if (prompt.getMessage() != null) {
            messageText.setText(prompt.getMessage());
        }

        if (prompt.getOkResId() != 0) {
            okButton.setText(prompt.getOkResId());
        }

        if (prompt.getCancelResId() != 0) {
            cancelButton.setText(prompt.getCancelResId());
        }

        if (prompt.getCancelState() == null) {
            cancelButton.setVisibility(GONE);
        }

        if (prompt.getOkState() == null) {
            okButton.setVisibility(GONE);
        }
    }

    @Override
    protected void doInject(ViewComponent injector) {
        injector.inject(this);
    }
}
