package com.example.app.view.base.adapter;

import com.example.app.dao.SecureTextDao;
import com.example.app.entity.SecureText;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by yinzara on 8/13/15.
 */
public class SimpleSecureTextAdapter extends StringEntityAdapter<SecureText> {

    @Inject
    SecureTextDao secureTextDao;

    @Inject
    public SimpleSecureTextAdapter() {
        super(android.R.layout.simple_list_item_1);
    }

    @Override
    protected List<SecureText> loadEntities(int offset, int limit) {
        return secureTextDao
                .queryBuilder()
                .orderDesc(SecureTextDao.Properties.Sent)
                .offset(offset)
                .limit(limit)
                .list();
    }
}
