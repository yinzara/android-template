package com.example.app.view.app.login;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.app.dagger.component.ViewComponent;
import com.example.app.mediator.LoginMediator;
import com.example.app.view.base.BaseLinearLayout;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by yinzara on 8/14/15.
 */
@EViewGroup
public class LoginView extends BaseLinearLayout {

    @Bean
    LoginViewPresenter presenter;

    @Bean
    LoginMediator loginBS;

    @ViewById
    EditText usernameText;

    @ViewById
    EditText pinText;

    @ViewById
    TextView errorMessageText;

    @ViewById
    Button loginButton;

    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public LoginViewPresenter getPresenter() {
        return presenter;
    }

    void setErrorMessageText(final String text) {
        errorMessageText.setText(text);
        loginButton.setEnabled(true);
    }

    @Click
    void loginButton() {
        final String username = usernameText.getText().toString();
        final String pin = pinText.getText().toString();
        presenter.doLogin(username, pin);
        loginButton.setEnabled(false);
    }

    @Override
    protected void doInject(ViewComponent injector) {
        injector.inject(this);
    }
}
