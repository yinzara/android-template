package com.example.app.view.base;

import com.example.app.view.Paths;

public interface IsMasterView {
    void updateSelection(Paths.MasterDetailPath newPath);
}
