package com.example.app.view.base.adapter;

import android.os.Parcelable;
import android.view.View;
import android.widget.ListAdapter;

import de.greenrobot.dao.HasId;
import com.example.app.event.EntityEvent;
import com.example.app.event.EntityListEvent;

/**
 * Created by yinzara on 8/19/15.
 */
public interface EntityAdapter<T extends HasId & Parcelable, V extends View> extends ListAdapter {

    void onEvent(EntityEvent<T> event, Runnable runAfter);

    void onEvent(EntityListEvent<T> event, Runnable runAfter);
}
