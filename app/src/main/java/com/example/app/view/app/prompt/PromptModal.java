package com.example.app.view.app.prompt;

import android.support.annotation.StringRes;

import com.example.app.R;
import com.example.app.view.base.pathview.Layout;
import com.example.app.view.Paths;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Created by yinzara on 8/17/15.
 */
@Layout(R.layout.prompt_view)
@NoArgsConstructor
@Accessors(chain = true)
public class PromptModal extends Paths.ModalDialog {

    public static final Object EXIT_STATE = new Object();

    public static final Object BACK_STATE = new Object();

    @Getter
    @Setter
    private int messageResId;

    @Getter
    @Setter
    private String message;

    @Getter
    @Setter
    private Object okState = BACK_STATE;

    @Getter
    private Object cancelState;

    @Getter
    @Setter
    private int okResId = R.string.ok;

    @Getter
    @Setter
    private int cancelResId = R.string.cancel;

    public static PromptModal confirmExit() {
        return confirm(R.string.confirm_exit, EXIT_STATE);
    }

    public static PromptModal confirm(@StringRes int messageId, Object okState) {
        return new PromptModal()
                .setMessageResId(messageId)
                .setOkState(okState)
                .setCancelState(BACK_STATE);
    }

    public PromptModal setCancelState(Object cancelState) {
        this.cancelState = cancelState;
        if (cancelState == BACK_STATE && okState == BACK_STATE) {
            this.okState = null;
        }
        return this;
    }
}