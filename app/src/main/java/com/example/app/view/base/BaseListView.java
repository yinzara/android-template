package com.example.app.view.base;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

import com.squareup.otto.Bus;
import com.example.app.VSTApplication;
import com.example.app.dagger.component.ViewComponent;

import javax.inject.Inject;

import mortar.Presenter;

/**
 * Created by yinzara on 8/12/15.
 */
public abstract class BaseListView extends ListView implements PresenterView {

    @Inject
    protected Bus bus;

    public BaseListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inject();
    }

    private final void inject() {
        doInject(VSTApplication.injector(this));
    }

    /**
     * If child class has any @Inject fields, must override this method (just copy and paste the entire thing as the body is already right for the child class)
     * @param inject
     */
    protected abstract void doInject(ViewComponent inject);

    public Presenter<? extends BaseListView> getPresenter() {
        return null;
    }

    @Override
    @SuppressWarnings("rawtypes")
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        bus.register(this);
        final Presenter presenter = getPresenter();
        if (presenter != null) {
            presenter.takeView(this);
        }
    }

    @Override
    @SuppressWarnings("rawtypes")
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        bus.unregister(this);
        final Presenter presenter = getPresenter();
        if (presenter != null) {
            presenter.takeView(this);
        }
    }
}
