package com.example.app.view.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;

import com.example.app.dagger.component.ViewComponent;
import com.example.app.view.Paths;
import com.example.app.R;
import com.example.app.view.base.pathview.BackSupport;
import com.example.app.view.base.pathview.FramePathContainerView;
import com.example.app.view.base.pathview.HandlesBack;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import flow.Flow;
import flow.path.Path;
import flow.path.PathContainerView;
import mortar.Presenter;

/**
 * This view is shown only in landscape orientation on tablets. See
 * the explanation in {@link com.example.app.MainActivity#onCreate}.
 */
@EViewGroup
public class TabletMasterDetailRoot extends BaseLinearLayout implements HandlesBack, PathContainerView {

    @ViewById(R.id.master)
    FramePathContainerView masterContainer;

    @ViewById(R.id.detail)
    FramePathContainerView detailContainer;

    private boolean disabled;

    public TabletMasterDetailRoot(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return !disabled && super.dispatchTouchEvent(ev);
    }

    @Override
    public ViewGroup getCurrentChild() {
        Paths.MasterDetailPath showing = Path.get(getContext());
        return showing.isMaster() ? masterContainer.getCurrentChild()
                : detailContainer.getCurrentChild();
    }

    @Override
    public ViewGroup getContainerView() {
        return this;
    }

    @Override
    public void dispatch(final Flow.Traversal traversal, Flow.TraversalCallback callback) {

        class CountdownCallback implements Flow.TraversalCallback {
            final Flow.TraversalCallback wrapped;
            int countDown = 2;

            CountdownCallback(Flow.TraversalCallback wrapped) {
                this.wrapped = wrapped;
            }

            @Override
            public void onTraversalCompleted() {
                countDown--;
                if (countDown == 0) {
                    disabled = false;
                    wrapped.onTraversalCompleted();
                    final ViewGroup child = masterContainer.getCurrentChild();
                    if (child instanceof IsMasterView) {
                        ((IsMasterView) child).updateSelection(
                                traversal.destination.<Paths.MasterDetailPath>top());
                    } if (child instanceof PresenterView) {
                        final Presenter<?> presenter = ((PresenterView)child).getPresenter();
                        if (presenter instanceof IsMasterView) {
                            ((IsMasterView)presenter).updateSelection(traversal.destination.<Paths.MasterDetailPath>top());
                        }
                    }
                }
            }
        }

        disabled = true;
        callback = new CountdownCallback(callback);
        detailContainer.dispatch(traversal, callback);
        masterContainer.dispatch(traversal, callback);
    }

    @Override
    public boolean onBackPressed() {
        return BackSupport.onBackPressed(detailContainer);
    }

    @Override
    protected void doInject(ViewComponent injector) {

    }
}
