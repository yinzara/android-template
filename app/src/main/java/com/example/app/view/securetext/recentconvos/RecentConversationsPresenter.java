package com.example.app.view.securetext.recentconvos;

import android.os.Bundle;

import com.example.app.dagger.component.PresenterComponent;
import com.example.app.entity.Conversation;
import com.example.app.mediator.SecureTextMediator;
import com.example.app.view.Paths;
import com.example.app.view.base.IsMasterView;
import com.example.app.view.base.adapter.EntityAdapterOnScrollListener;
import com.example.app.view.base.presenter.ScreenPresenter;
import com.example.app.view.securetext.conversation.ConversationScreen;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.List;

/**
 * Created by yinzara on 8/17/15.
 */
@EBean
public class RecentConversationsPresenter extends ScreenPresenter<RecentConversationsView> implements IsMasterView {

    @Bean
    SecureTextMediator mediator;

    public void onConversationSelected(Conversation convo) {
        flow.set(new ConversationScreen(convo.getId()));
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        mediator.updateSecureTexts();
        getView().setOnScrollListener(new EntityAdapterOnScrollListener(getView().getEntityAdapter()));
    }

    @Override
    public void updateSelection(Paths.MasterDetailPath newPath) {
        if (!hasView()) return;
        final Paths.ConversationPath path = (Paths.ConversationPath) newPath;
        final RecentConversationsView view = getView();
        final List<Conversation> conversations = view.getEntityAdapter().getEntities();
        for (int i = 0; i < conversations.size(); i++) {
            if (conversations.get(i).getId() == path.getConversationId()) {
                view.setItemChecked(i, true);
                break;
            }
        }
        view.invalidate();
    }

    @Override
    protected void doInject(PresenterComponent injector) {
        injector.inject(this);
    }
}
