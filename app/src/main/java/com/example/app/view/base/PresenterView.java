package com.example.app.view.base;

import mortar.Presenter;

/**
 * Created by yinzara on 8/17/15.
 */
public interface PresenterView {

    public <T extends Presenter<?>> T getPresenter();
}
