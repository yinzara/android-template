package com.example.app.view.app.splash;

import android.content.Context;
import android.util.AttributeSet;

import com.example.app.dagger.component.ViewComponent;
import com.example.app.view.app.login.LoginScreen;
import com.example.app.view.securetext.recentconvos.RecentConversationsScreen;
import com.example.app.mediator.LoginMediator;
import com.example.app.view.base.BaseRelativeLayout;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;

import flow.Flow;
import flow.History;

/**
 * Created by yinzara on 8/14/15.
 */
@EViewGroup
public class SplashView extends BaseRelativeLayout {

    @Bean
    LoginMediator loginBS;

    private static final int MOVE_TO_NEXT_SCREEN_DELAY = 2000;

    public SplashView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void doInject(ViewComponent injector) {
        injector.inject(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        moveToNextScene();
    }

    @UiThread(delay = MOVE_TO_NEXT_SCREEN_DELAY)
    void moveToNextScene() {
        final Object state;
        if (loginBS.isLoggedIn()) {
            state = new RecentConversationsScreen();
        } else {
            state = new LoginScreen();
        }
        Flow.get(SplashView.this).setHistory(History.single(state), Flow.Direction.REPLACE);
    }
}
