package com.example.app.view.base.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.view.View;

import de.greenrobot.dao.HasId;
import com.example.app.view.base.EntityBindable;

/**
 * Created by yinzara on 8/14/15.
 */
public abstract class BaseEntityBindableViewCursorAdapter<T extends HasId & Parcelable, V extends View & EntityBindable<T>> extends BaseEntityCursorAdapter<T, V> {

    public BaseEntityBindableViewCursorAdapter(final Context context, @LayoutRes final int resourceId) {
        super(context, resourceId);

    }

    @Override
    protected void bindView(V view, T t) {
        view.bind(t);
    }
}
