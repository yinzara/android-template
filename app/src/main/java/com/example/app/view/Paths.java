/*
 * Copyright 2013 Square Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.app.view;

import com.example.app.R;
import com.example.app.view.base.TabletMasterDetailRoot;
import com.example.app.view.base.pathview.Layout;
import com.example.app.view.securetext.conversation.ConversationScreen;
import com.example.app.view.securetext.message.MessageScreen;
import com.example.app.view.securetext.recentconvos.RecentConversationsScreen;

import flow.path.Path;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * All paths must have a no args constructor and all properties must be settable via properties<br/>
 * This is because each path is used as state and it must be able to be serialized and deserialized by Jackson<br/>
 * The setters on these paths should never be called once they have been set as state for an object (i.e. never from history)<br/>
 * Always construct a new path when calling flow.set or history.push
 */
public final class Paths {

    @Layout(R.layout.no_details)
    public static class NoDetails extends Path {
    }

    /**
     * Identifies screens in a master / detail relationship. Both master and detail screens
     * extend this class.
     * <p/>
     * Not a lot of thought has been put into making a decent master / detail modeling here. Rather
     * this is an excuse to show off using Flow to build a responsive layout. See {@link
     * TabletMasterDetailRoot}.
     */
    public abstract static class MasterDetailPath extends Path {
        /**
         * Returns the screen that shows the master list for this type of screen.
         * If this screen is the master, returns self.
         * <p/>
         * For example, the {@link ConversationScreen} and {@link MessageScreen} screens are both
         * "under" the master {@link RecentConversationsScreen} screen. All three of these
         * screens return a {@link ConversationScreen} from this method.
         */
        public abstract MasterDetailPath getMaster();

        public final boolean isMaster() {
            return equals(getMaster());
        }
    }

    @EqualsAndHashCode(callSuper=false)
    @NoArgsConstructor
    @AllArgsConstructor
    public abstract static class ConversationPath extends MasterDetailPath {
        @Getter
        @Setter
        private long conversationId = -1;

        @Override
        public MasterDetailPath getMaster() {
            return new RecentConversationsScreen();
        }
    }

    /**
     * Identifies all paths that show as a full screen modal
     */
    public abstract static class ModalDialog extends NoDetails {

    }

    private Paths() {
    }
}
