package com.example.app.view.app.login;

import com.squareup.otto.Subscribe;
import com.example.app.dagger.component.PresenterComponent;
import com.example.app.event.global.remote.LoginEvent;
import com.example.app.mediator.LoginMediator;
import com.example.app.view.base.pathview.HandlesBack;
import com.example.app.view.base.presenter.ScreenPresenter;
import com.example.app.view.app.prompt.PromptModal;
import com.example.app.view.securetext.recentconvos.RecentConversationsScreen;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import flow.Flow;
import flow.History;

/**
 * Created by yinzara on 8/19/15.
 */
@EBean
public class LoginViewPresenter extends ScreenPresenter<LoginView> implements HandlesBack {

    @Bean
    LoginMediator loginMediator;

    @Subscribe
    public void onLoginEvent(LoginEvent loginEvent) {
        if (!hasView())  return;
        final LoginView view = getView();
        if (loginEvent.isSuccess()) {
            final Flow flow = Flow.get(view);
            //If there is a flow on the backstack it just means we got sent to the login view and we should return to wherever we were
            if (!flow.goBack()) {
                flow.setHistory(History.emptyBuilder().push(new RecentConversationsScreen()).build(), Flow.Direction.REPLACE);
            }
        } else {
            view.setErrorMessageText(loginEvent.getMessage());
        }
    }

    void doLogin(final String username, final String pin) {
        loginMediator.doLogin(username, pin);
    }

    @Override
    public boolean onBackPressed() {
        if (!hasView())  return false;
        final LoginView view = getView();

        Flow.get(view).set(PromptModal.confirmExit());
        return true;
    }

    @Override
    protected void doInject(PresenterComponent injector) {
        injector.inject(this);
    }
}
