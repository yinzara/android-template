package com.example.app.view.base.adapter;

import android.os.SystemClock;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

import java.lang.ref.WeakReference;

import javax.validation.constraints.NotNull;

/**
 * Created by yinzara on 8/19/15.
 */
public class EntityAdapterOnScrollListener implements OnScrollListener {

    private static final int LOAD_WAIT = 5000;

    private WeakReference<BaseEntityAdapter<?, ?>> reference;

    private OnScrollListener listener;

    private long lastLoadTime = 0;

    public EntityAdapterOnScrollListener(@NotNull BaseEntityAdapter<?, ?> adapter, OnScrollListener otherListener) {
        this.reference = new WeakReference<>(adapter);
        this.listener = otherListener;
    }

    public EntityAdapterOnScrollListener(@NotNull BaseEntityAdapter<?, ?> adapter) {
        this(adapter, null);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (listener != null) {
            listener.onScrollStateChanged(view, scrollState);
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
            if (lastLoadTime == 0 || lastLoadTime + LOAD_WAIT > SystemClock.elapsedRealtime()) {
                final BaseEntityAdapter<?, ?> adapter = reference.get();
                lastLoadTime = SystemClock.elapsedRealtime();
                if (adapter != null) {
                    adapter.loadMore(() -> {
                        if (adapter.getLastLoadCount() > 0) {
                            lastLoadTime = 0;
                        }
                    });
                }

            }
        }
        if (listener != null) {
            listener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        }
    }
}
