package com.example.app.view.base.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.widget.ResourceCursorAdapter;

import com.example.app.dao.ConversationDao;
import com.example.app.dao.DaoSession;

import de.greenrobot.dao.HasId;

import com.example.app.event.EntityEvent;
import com.example.app.event.EntityListEvent;
import com.example.app.util.Utils;

import org.androidannotations.api.BackgroundExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.WhereCondition;

/**
 * Created by yinzara on 8/19/15.
 */
public abstract class BaseEntityCursorAdapter<EntityType extends HasId & Parcelable, ViewType extends View> extends ResourceCursorAdapter implements EntityAdapter<EntityType, ViewType> {

    private static final Map<Class<?>, String[]> COL_MAP = new ConcurrentHashMap();

    protected Class<EntityType> entityClass = Utils.getGenericSuperclassParam(getClass(), 0);

    @Inject
    protected Context context;

    @Inject
    protected DaoSession session;

    private boolean querying;

    private Handler handler = new Handler(Looper.getMainLooper());

    private String[] cols;

    public BaseEntityCursorAdapter(Context context, @LayoutRes int layout) {
        super(context, layout, null, 0);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        EntityType entity;
        final AbstractDao<EntityType, ?> dao = (AbstractDao<EntityType, ?>) session.getDao(entityClass);
        entity = dao.readEntity(cursor, 0);
        bindView((ViewType) view, entity);
    }

    protected abstract void bindView(ViewType view, EntityType entityType);

    protected abstract Cursor query();

    protected abstract DaoConfig getDaoConfig();

    protected Cursor query(final String selection) {
        return query(selection, (String[]) null);
    }

    protected Cursor query(final String orderBy, final WhereCondition ... wheres) {
        final StringBuilder buf = new StringBuilder();
        final List<Object> args = wheres != null?new ArrayList<Object>(wheres.length):null;

        if (wheres != null) {
            for (int i = 0; i < wheres.length; i++) {
                if (i != 0) {
                    buf.append(" AND ");
                }

                if (wheres[i] instanceof WhereCondition.PropertyCondition) {
                    buf.append('"');
                    buf.append(((WhereCondition.PropertyCondition)wheres[i]).property.columnName);
                    buf.append('"');
                    buf.append(((WhereCondition.PropertyCondition)wheres[i]).op);
                } else if (wheres[i] instanceof WhereCondition.StringCondition) {
                    wheres[i].appendTo(buf);
                }
                wheres[i].appendValuesTo(args);
            }
        }
        final String[] strArgs = wheres != null?new String[args.size()]:null;
        if (strArgs != null) {
            for (int i = 0; i < strArgs.length; i++) {
                strArgs[i] = args.get(i).toString();
            }
        }
        return query(buf.toString(), strArgs, orderBy);
    }

    protected Cursor query(final String selection, final String orderBy) {
        return query(selection, null, orderBy);
    }

    protected Cursor query(final String selection, final String[] selectionArgs) {
        return query(selection, selectionArgs, null);
    }

    protected Cursor query(final String selection, final String[] selectionArgs, String orderBy) {
        return query(selection, selectionArgs, null, null, orderBy);
    }

    protected Cursor query(final String selection, final String[] selectionArgs, String groupBy, String having) {
        return query(selection, selectionArgs, groupBy, having, null);
    }

    protected Cursor query(final String selection, final String[] selectionArgs, String groupBy, String having, String orderBy) {
        return query(false, selection, selectionArgs, groupBy, having, orderBy, null);
    }

    protected Cursor query(boolean distinct, final String selection, final String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        return session.getDatabase().query(distinct, getTableName(), getColumnNames(), selection, selectionArgs, groupBy, having, orderBy, limit);
    }

    protected String getTableName() {
        return getDaoConfig().tablename;
    }

    protected String[] getColumnNames() {
        if (cols == null) {
            cols = getColumnNames(getDaoConfig(), entityClass);
        }
        return cols;
    }

    static String[] getColumnNames(DaoSession session, Class<?> entityClass) {
        return getColumnNames(session.getDaoConfig(entityClass), entityClass);
    }

    protected static String[] getColumnNames(DaoConfig config, Class<?> entityClass) {
        String[] cols = COL_MAP.get(entityClass);
        if (cols == null) {
            cols = new String[config.allColumns.length];
            for (int i = 0; i < cols.length; i++) {
                final String col = config.allColumns[i];
                cols[i] = new StringBuilder(col.length() + 2).append('"').append(col).append('"').toString();
            }
            COL_MAP.put(entityClass, cols);
        }
        return cols;
    }

    private void doQuery() {
        querying = true;
        BackgroundExecutor.execute(() -> {
            final Cursor cursor = query();
            handler.post(() -> {
                changeCursor(cursor);
                querying = false;
            });
        });
    }

    @Override
    public int getCount() {
        if (getCursor() == null) {
            synchronized (this) {
                if (!querying) {
                    doQuery();
                }
            }
            return 0;
        } else {
            return super.getCount();
        }
    }

    @Override
    public void onEvent(EntityEvent<EntityType> event, Runnable runAfter) {
        doQuery();
    }

    @Override
    public void onEvent(EntityListEvent<EntityType> event, Runnable runAfter) {
        doQuery();
    }
}
