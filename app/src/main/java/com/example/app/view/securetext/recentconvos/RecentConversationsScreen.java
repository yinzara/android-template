package com.example.app.view.securetext.recentconvos;

import com.example.app.R;
import com.example.app.view.base.pathview.Layout;
import com.example.app.view.Paths;
import com.example.app.view.base.pathview.Title;

/**
 * Created by yinzara on 8/17/15.
 */
@Title(R.string.recent_conversations_screen)
@Layout(R.layout.recent_conversations_view) //
public class RecentConversationsScreen extends Paths.ConversationPath {

}
