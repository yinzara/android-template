package com.example.app.view.base.adapter;

import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.view.ViewGroup;

import de.greenrobot.dao.HasId;
import com.example.app.view.base.EntityBindable;

/**
 * Created by yinzara on 8/14/15.
 */
public abstract class BaseEntityBindableViewAdapter<T extends HasId & Parcelable, V extends View & EntityBindable<T>> extends BaseEntityAdapter<T, V> {

    private final Binder<T, V> binder = (T entity, V view) -> {
        view.bind(entity);
    };

    @LayoutRes
    private final int itemResourceId;

    public BaseEntityBindableViewAdapter(@LayoutRes final int itemResourceId) {
        this.itemResourceId = itemResourceId;
    }

    @Override
    public Binder<T, V> getBinder() {
        return binder;
    }

    @Override
    protected V buildNewView(ViewGroup parent) {
        return createViewFromResource(itemResourceId, parent);
    }
}
