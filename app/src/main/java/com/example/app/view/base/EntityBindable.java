package com.example.app.view.base;

import de.greenrobot.dao.HasId;

/**
 * Created by yinzara on 8/14/15.
 */
public interface EntityBindable<T extends HasId> {

    void bind(T entity);
}
