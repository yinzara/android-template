package com.example.app.view.securetext.conversation;

import com.example.app.R;
import com.example.app.view.base.pathview.Layout;
import com.example.app.view.Paths;

import lombok.NoArgsConstructor;

/**
 * Created by yinzara on 8/17/15.
 */
@Layout(R.layout.conversation_view) //
@NoArgsConstructor
public class ConversationScreen extends Paths.ConversationPath {
    public ConversationScreen(long conversationId) {
        super(conversationId);
    }
}
