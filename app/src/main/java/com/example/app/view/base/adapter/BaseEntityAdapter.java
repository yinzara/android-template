package com.example.app.view.base.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.greenrobot.dao.HasId;

import com.example.app.dagger.ViewScoped;
import com.example.app.event.EntityEvent;
import com.example.app.event.EntityListEvent;

import org.androidannotations.api.BackgroundExecutor;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

/**
 * Base adapter for all entity related list views<br/>
 * <p>
 * Created by yinzara on 8/13/15.
 */
public abstract class BaseEntityAdapter<T extends HasId & Parcelable, V extends View> extends android.widget.BaseAdapter implements EntityAdapter<T,V> {

    private static final int DEFAULT_LIMIT = 30;

    public interface Binder<T extends HasId, V extends View> {
        void bind(T entity, V view);
    }

    @Inject
    protected Context context;

    @Inject
    LayoutInflater mInflater;

    private Handler handler;

    private int offset;

    private int limit = DEFAULT_LIMIT;

    private int lastLoadCount;

    private List<T> entities;

    private boolean querying;

    public BaseEntityAdapter() {
        super();
    }

    protected abstract V buildNewView(ViewGroup parent);

    protected abstract Binder<T, V> getBinder();

    protected abstract List<T> loadEntities(int offset, int limit);

    private void queryNext(final boolean reset) {
        queryNext(reset, null);
    }

    private synchronized void queryNext(final boolean reset, final Runnable runWhenUpdateComplete) {
        if (querying) {
            return;
        }
        if (reset) {
            limit = DEFAULT_LIMIT;
            offset = 0;
        }
        querying = true;
        runBackground(() ->
                {
                    try {
                        final List<T> newEntities = loadEntities(offset, limit);
                        lastLoadCount = newEntities.size();
                        if (reset) {
                            entities = new LinkedList(newEntities);
                        } else {
                            entities.addAll(newEntities);
                        }
                        offset = limit + offset;
                        if (!reset || !entities.isEmpty()) {
                            onEntitiesLoaded();
                            runOnUiThread(() -> {
                                notifyDataSetInvalidated();
                            });

                        }
                        querying = false;
                        if (runWhenUpdateComplete != null) {
                            runOnUiThread(runWhenUpdateComplete);
                        }
                    } catch (Throwable e) {
                        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                    }
                }
        );
    }

    public void loadMore(Runnable doAfter) {
        queryNext(false, doAfter);
    }

    /**
     * Always called on the background thread
     */
    protected void onEntitiesLoaded() {

    }

    /**
     * Overwrite in child classes and return true if the list of items should be displayed in reverse order
     *
     * @return
     */
    protected boolean isReversed() {
        return false;
    }

    /**
     * Run the given runnable on the UI thread
     * @param r
     */
    protected final void runOnUiThread(final Runnable r) {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        handler.post(r);
    }

    /**
     * Run the given runnable on the background thread
     * @param r
     */
    protected final void runBackground(final Runnable r) {
        BackgroundExecutor.execute(r);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        V view;
        if (convertView != null) {
            view = (V) convertView;
        } else {
            view = buildNewView(parent);
        }
        final T entity = entities.get(isReversed() ? (getCount() - position - 1) : position);
        getBinder().bind(entity, view);

        return view;
    }

    @Override
    public int getCount() {
        if (entities == null) {
            entities = new LinkedList<T>();
            queryNext(false);
        }
        return entities.size();
    }

    @Override
    public T getItem(int position) {
        return entities.get(position);
    }

    @Override
    public long getItemId(int position) {
        final Long id = entities.get(position).getId();
        if (id == null) {
            return position * -1;
        } else {
            return id.longValue();
        }
    }

    /**
     * This method should be called from the view that is using this adapter<br/>
     * The view should have a method annotated with @Subscribe that's has a single parameter of the type EntityEvent of the entity this adapter uses<br/>
     *
     * This method MUST be called from the UIThread
     * @param event
     */
    public final void onEvent(final EntityEvent<T> event) {
        onEvent(event, null);
    }

    /**
     * This method MUST be called from the UI thread
     * @param event
     * @param runWhenUpdateComplete
     */
    public void onEvent(final EntityEvent<T> event, final Runnable runWhenUpdateComplete) {
        switch (event.getType()) {
            case UPDATE:
                int i;
                for (i = 0; i < entities.size(); i++) {
                    if (entities.get(i).getId() == event.getEntity().getId()) {
                        entities.set(i, event.getEntity());
                        notifyDataSetChanged();
                        if (runWhenUpdateComplete != null) {
                            runOnUiThread(runWhenUpdateComplete);
                        }
                        break;
                    }
                }
                break;
            case INSERT:
                queryNext(true, runWhenUpdateComplete);
                break;
            case DELETE:
                for (i = 0; i < entities.size(); i++) {
                    if (entities.get(i).getId() == event.getEntity().getId()) {
                        entities.remove(i);
                        notifyDataSetChanged();
                        if (runWhenUpdateComplete != null) {
                            runOnUiThread(runWhenUpdateComplete);
                        }
                        break;
                    }
                }
                break;
        }
    }

    public final void onEvent(final EntityListEvent<T> event) {
        onEvent(event, null);
    }

    public void onEvent(final EntityListEvent<T> event, Runnable runWhenDone) {
        if (!event.getInserted().isEmpty()) {
            queryNext(true, runWhenDone);
        } else {
            boolean dataSetChanged = false;
            for (final T entity : event.getUpdated()) {
                int i;
                for (i = 0; i < entities.size(); i++) {
                    if (entities.get(i).getId() == entity.getId()) {
                        entities.set(i, entity);
                        dataSetChanged = true;
                        break;
                    }
                }
            }

            for (Long id : event.getDeleted()) {
                for (int i = 0; i < entities.size(); i++) {
                    if (entities.get(i).getId() == id) {
                        entities.remove(i);
                        dataSetChanged = true;
                        break;
                    }
                }
            }

            if (dataSetChanged) {
                notifyDataSetChanged();
            }
            if (runWhenDone != null) {
                runOnUiThread(runWhenDone);
            }
        }
    }

    public List<T> getEntities() {
        return Collections.unmodifiableList(entities);
    }

    public int getLastLoadCount() {
        return lastLoadCount;
    }

    protected final <V extends View> V createViewFromResource(@LayoutRes int resource, ViewGroup parent) {
        return createViewFromResource(resource, 0, parent);
    }

    protected final <V extends View> V createViewFromResource(@LayoutRes int resource, @IdRes int fieldId, ViewGroup parent) {
        View view = mInflater.inflate(resource, parent, false);

        try {
            if (fieldId != 0) {
                view = view.findViewById(fieldId);
            }
        } catch (ClassCastException e) {
            Log.e("ArrayAdapter", "You must supply a resource ID for a TextView");
            throw new IllegalStateException(
                    "ArrayAdapter requires the resource ID to be a TextView", e);
        }

        return (V) view;
    }
}
