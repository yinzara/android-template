package com.example.app.view.base.presenter;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.squareup.otto.Bus;
import com.example.app.VSTApplication;
import com.example.app.dagger.component.PresenterComponent;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import javax.inject.Inject;

import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;

/**
 * Abstract base presenter for displaying any screen<br/>
 * To create a new presenter for the view "MyNewView":
 * <ol>
 *     <li>Create a class that extends from ScreenPresenter&lt;MyNewView&gt; named "MyNewViewPresenter"</li>
 *     <li>Add the @EBean annotation to MyNewViewPresenter class</li>
 *     <li>Override the "doInject" method with the body:<br/>@Override protected void doInject() { VSTApplication.injector(context).presenter().inject(this); }</li>
 *     <li>Edit the PresenterComponent class and add a method "void inject(MyNewView view);"</li>
 *     <li>Add a field to MyNewView:<br/>@Bean MyNewViewPresenter presenter;</li>*
 *     <li>Override getPresenter() method in MyNewView:@Override protected Presenter&lt;MyNewView&gt; getPresenter() { return presenter; }</li>
 * </ol>
 * Note: MyNewView must extend from one of the classes in com.example.app.view.base
 * Created by yinzara on 8/17/15.
 */
@EBean
public abstract class ScreenPresenter<T extends View> extends ViewPresenter<T> {

    @RootContext
    protected Context context;

    @Inject
    protected Bus bus;

    protected Flow flow;

    @AfterInject
    protected final void afterInject() {
        doInject(VSTApplication.injector(context).presenter());
    }

    protected abstract void doInject(PresenterComponent component);

    @Override
    protected void onEnterScope(MortarScope scope) {
        bus.register(this);
        final View view = getView();
        if (view != null) {
            flow = Flow.get(view);
        }
    }

    @Override
    protected void onExitScope() {
        bus.unregister(this);
    }


}
