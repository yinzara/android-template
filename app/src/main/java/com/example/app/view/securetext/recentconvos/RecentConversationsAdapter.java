package com.example.app.view.securetext.recentconvos;

import com.example.app.dao.ConversationDao;
import com.example.app.entity.Conversation;
import com.example.app.entity.view.adapter.ConversationListAdapter;
import com.example.app.entity.view.item.ConversationListItemView;
import com.example.app.util.DBUtils;
import com.example.app.view.base.adapter.BaseEntityAdapter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by yinzara on 8/19/15.
 */
public class RecentConversationsAdapter extends ConversationListAdapter {

    @Inject
    public RecentConversationsAdapter() {
        super();
    }

    @Override
    protected List<Conversation> loadEntities(int offset, int limit) {
        return _ConversationDao.queryDeep(
                DBUtils.orderDesc(true, ConversationDao.Properties.LastActivity)
                , offset, limit);
    }


}
