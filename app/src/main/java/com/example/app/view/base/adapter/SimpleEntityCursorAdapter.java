package com.example.app.view.base.adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.widget.SimpleCursorAdapter;

import com.example.app.dao.ConversationDao;
import com.example.app.dao.DaoSession;
import com.example.app.event.EntityEvent;
import com.example.app.event.EntityListEvent;
import com.example.app.util.Utils;

import org.androidannotations.api.BackgroundExecutor;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import de.greenrobot.dao.HasId;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.WhereCondition;

/**
 * Created by yinzara on 8/19/15.
 */
public abstract class SimpleEntityCursorAdapter<EntityType extends HasId & Parcelable> extends SimpleCursorAdapter implements EntityAdapter<EntityType,View> {

    protected Class<EntityType> entityClass = Utils.getGenericSuperclassParam(getClass(), 0);

    private DaoConfig daoConfig;

    private Handler handler = new Handler(Looper.getMainLooper());

    @Inject
    protected Context context;

    @Inject
    protected DaoSession session;

    private boolean querying;

    /**
     * @see SimpleCursorAdapter#SimpleCursorAdapter(Context, int, Cursor, String[], int[], int)
     * @param context
     * @param layout
     * @param from
     * @param to
     * @param flags
     */
    public SimpleEntityCursorAdapter(Context context, @LayoutRes int layout, String[] from, @IdRes int[] to, int flags) {
        super(context, layout, null, from, to, flags);
    }

    protected abstract Cursor query();

    protected Cursor query(final String selection) {
        return query(selection, (String[])null);
    }

    protected Cursor query(final String orderBy, final WhereCondition ... wheres) {
        final StringBuilder buf = new StringBuilder();
        final List<Object> args = wheres != null?new ArrayList<Object>(wheres.length):null;

        if (wheres != null) {
            for (int i = 0; i < wheres.length; i++) {
                if (i != 0) {
                    buf.append(" AND ");
                }

                if (wheres[i] instanceof WhereCondition.PropertyCondition) {
                    buf.append('"');
                    buf.append(((WhereCondition.PropertyCondition)wheres[i]).property.columnName);
                    buf.append('"');
                    buf.append(((WhereCondition.PropertyCondition)wheres[i]).op);
                } else if (wheres[i] instanceof WhereCondition.StringCondition) {
                    wheres[i].appendTo(buf);
                }
                wheres[i].appendValuesTo(args);
            }
        }
        final String[] strArgs = wheres != null?new String[args.size()]:null;
        if (strArgs != null) {
            for (int i = 0; i < strArgs.length; i++) {
                strArgs[i] = args.get(i).toString();
            }
        }
        return query(buf.toString(), strArgs, orderBy);
    }

    protected Cursor query(final String selection, final String[] selectionArgs) {
        return query(selection, selectionArgs, null);
    }

    protected Cursor query(final String selection, final String[] selectionArgs, String orderBy) {
        return query(selection, selectionArgs, null, null, orderBy);
    }

    protected Cursor query(final String selection, final String[] selectionArgs, String groupBy, String having) {
        return query(selection, selectionArgs, groupBy, having, null);
    }

    protected Cursor query(final String selection, final String[] selectionArgs, String groupBy, String having, String orderBy) {
        return query(false, selection, selectionArgs, groupBy, having, orderBy, null);
    }

    protected Cursor query(boolean distinct, final String selection, final String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        return session.getDatabase().query(distinct, getDaoConfig().tablename, getColumnNames(), selection, selectionArgs, groupBy, having, orderBy, limit);
    }

    protected String[] getColumnNames() {
        return BaseEntityCursorAdapter.getColumnNames(getDaoConfig(), entityClass);
    }

    protected DaoConfig getDaoConfig() {
        if (daoConfig == null) {
            daoConfig = session.getDaoConfig(entityClass);
        }
        return daoConfig;
    }

    private synchronized void doQuery() {
        if (!querying) {
            querying = true;
            BackgroundExecutor.execute(() -> {
                final Cursor cursor = query();
                handler.post(() -> {
                    changeCursor(cursor);
                    querying = false;
                });
            });
        }
    }

    @Override
    public int getCount() {
        if (getCursor() == null) {
            doQuery();
            return 0;
        } else {
            return super.getCount();
        }
    }

    @Override
    public void onEvent(EntityEvent<EntityType> event, Runnable runAfter) {
        doQuery();
    }

    @Override
    public void onEvent(EntityListEvent<EntityType> event, Runnable runAfter) {
        doQuery();
    }
}
