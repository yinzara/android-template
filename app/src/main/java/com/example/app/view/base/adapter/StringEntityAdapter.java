package com.example.app.view.base.adapter;

import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.TextView;

import de.greenrobot.dao.HasId;

import javax.validation.constraints.NotNull;

/**
 * Created by yinzara on 8/13/15.
 */
public abstract class StringEntityAdapter<T extends HasId & Parcelable> extends BaseEntityAdapter<T, TextView> {

    @LayoutRes
    @NotNull
    private int mViewId;

    @IdRes
    private int mFieldId;

    private final Binder<T, TextView> binder = (T entity, TextView view) -> {
        if (view != null) {
            view.setText(StringEntityAdapter.this.toString(entity));
        }
    };

    public StringEntityAdapter(@NotNull @LayoutRes int viewResourceId) {
        this.mViewId = viewResourceId;
    }

    public StringEntityAdapter(@NotNull @LayoutRes int viewResourceId, @Nullable @IdRes int textViewId) {
        this.mViewId = viewResourceId;
        this.mFieldId = textViewId;
    }

    @Override
    protected TextView buildNewView(@Nullable ViewGroup parent) {
        return createViewFromResource(mViewId, mFieldId, parent);
    }

    @Override
    public Binder<T, TextView> getBinder() {
        return binder;
    }

    protected CharSequence toString(T entity) {
        if (entity instanceof CharSequence) {
            return (CharSequence) entity;
        } else {
            return entity.toString();
        }
    }
}
