package com.example.app.state;

import org.androidannotations.annotations.sharedpreferences.DefaultLong;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

import java.util.Date;

/**
 * Created by yinzara on 8/17/15.
 */
@SharedPref
public interface DataState {

    /**
     * The date/time last sync of secure text data was complete
     * @return
     */
    @DefaultLong(0L)
    long lastSecureTextSync();
}
