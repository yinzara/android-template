package com.example.app.state;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by yinzara on 8/17/15.
 */
@SharedPref
public interface AppPreferences {

    /**
     * @return the root URL used for API requests
     */
    @DefaultString("https://api.app.example.com")
    String restRootURL();

    @DefaultInt(0)
    int environmentId();

}
