package com.example.app.state;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultLong;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by yinzara on 8/14/15.
 */
@SharedPref
public interface LoginState {

    /**
     * @return x-prana-token used to authenticate all requests
     */
    String token();

    /**
     * @return the expiration time of the token in milliseconds since the epoch
     */
    @DefaultLong(0)
    long tokenExpiration();

    /**
     * The staff ID of the current logged in user or 0 if they are logged out
     * @return
     */
    @DefaultInt(0)
    int staffId();

}
