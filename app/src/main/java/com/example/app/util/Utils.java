/*
 * Copyright 2013 Square Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.app.util;

import android.view.View;
import android.view.ViewTreeObserver;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public final class Utils {

    public interface OnMeasuredCallback {
        void onMeasured(View view, int width, int height);
    }

    public static List<Long> toList(final long[] val) {
        if (val == null) {
            return new LinkedList<Long>();
        }
        final List<Long> vals = new ArrayList<>(val.length);
        for (int i = 0;i < val.length;i++) {
            vals.add(val[i]);
        }
        return vals;
    }

    public static long[] toArray(final List<Long> lst) {
        if (lst == null) {
            return new long[0];
        }
        final long[] arr = new long[lst.size()];
        for (int i = 0; i < arr.length;i++) {
            arr[i] = lst.get(i);
        }
        return arr;
    }

    public static long[] toArray(final Collection<Long> lst) {
        if (lst == null) {
            return new long[0];
        }
        final long[] arr = new long[lst.size()];
        int i = 0;
        for (final Long l : lst) {
            arr[i++] = l;
        }
        return arr;
    }

    public static boolean notEqual(int oldValue, int newValue) {
        return oldValue != newValue;
    }

    public static boolean notEqual(float oldValue, float newValue) {
        return oldValue != newValue;
    }

    public static boolean notEqual(boolean oldValue, boolean newValue) {
        return oldValue != newValue;
    }

    public static boolean notEqual(double oldValue, double newValue) {
        return oldValue != newValue;
    }

    public static boolean notEqual(long oldValue, long newValue) {
        return oldValue != newValue;
    }

    public static boolean notEqual(short oldValue, short newValue) {
        return oldValue != newValue;
    }

    public static <T> boolean notEqual(final T oldValue, final T newValue) {
        if (oldValue == null && newValue != null || oldValue != null && !oldValue.equals(newValue)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean newer(final Date oldValue, final Date newValue) {
        if (oldValue == null) {
            return newValue != null;
        } else if (newValue == null) {
            return false;
        } else {
            return newValue.after(oldValue);
        }
    }

    public static boolean older(final Date oldValue, final Date newValue) {
        if (oldValue == null) {
            return newValue != null;
        } else if (newValue == null) {
            return false;
        } else {
            return newValue.before(oldValue);
        }
    }

    public static void waitForMeasure(final View view, final OnMeasuredCallback callback) {
        int width = view.getWidth();
        int height = view.getHeight();

        if (width > 0 && height > 0) {
            callback.onMeasured(view, width, height);
            return;
        }

        view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                final ViewTreeObserver observer = view.getViewTreeObserver();
                if (observer.isAlive()) {
                    observer.removeOnPreDrawListener(this);
                }

                callback.onMeasured(view, view.getWidth(), view.getHeight());

                return true;
            }
        });
    }

    public static <EventType> Class<EventType> getGenericSuperclassParam(Class<?> clazz, int index) {

        while (clazz.getSuperclass() != null && clazz.getSuperclass() != Object.class) {
            try {
                java.lang.reflect.Type genericSuperclass = clazz.getGenericSuperclass();
                java.lang.reflect.Type type = null;
                if (genericSuperclass instanceof ParameterizedType) {
                    type = ((ParameterizedType) genericSuperclass).getActualTypeArguments()[index];
                }

                if (type instanceof Class) {
                    clazz = (Class<EventType>) type;
                    break;
                } else if (genericSuperclass instanceof Class){
                    clazz = (Class)genericSuperclass;
                } else {
                    clazz = clazz.getSuperclass();
                }
            } catch (ArrayIndexOutOfBoundsException exp) {
                throw new IllegalArgumentException("Generic superclass did not have " + index + 1 + " arguments");
            }
        }

        return (Class<EventType>) clazz;

    }

    private Utils() {
    }
}
