package com.example.app.util;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.app.rest.dto.CometDMessage;

import org.cometd.bayeux.Message;
import org.cometd.common.HashMapMessage;
import org.cometd.common.JSONContext;
import org.cometd.common.JSONContext.Client;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.text.ParseException;

/**
 * Created by yinzara on 8/20/15.
 */
public class JacksonJSONContext implements Client {
    private final ObjectMapper objectMapper;
    private final JavaType rootArrayType;

    protected JacksonJSONContext() {
        this(new ObjectMapper());
    }

    public JacksonJSONContext(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        rootArrayType = objectMapper.constructType(rootArrayClass());
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    private Class<CometDMessage[]> rootArrayClass() {
        return CometDMessage[].class;
    }

    public HashMapMessage[] parse(InputStream stream) throws ParseException {
        try {
            return getObjectMapper().readValue(stream, rootArrayType);
        } catch (IOException x) {
            throw (ParseException) new ParseException("", -1).initCause(x);
        }
    }

    public HashMapMessage[] parse(Reader reader) throws ParseException {
        try {
            return getObjectMapper().readValue(reader, rootArrayType);
        } catch (IOException x) {
            throw (ParseException) new ParseException("", -1).initCause(x);
        }
    }

    public HashMapMessage[] parse(String json) throws ParseException {
        try {
            return getObjectMapper().readValue(json, rootArrayType);
        } catch (IOException x) {
            throw (ParseException) new ParseException(json, -1).initCause(x);
        }
    }


    public String generate(Message.Mutable message) {
        try {
            return getObjectMapper().writeValueAsString(message);
        } catch (IOException x) {
            throw new RuntimeException(x);
        }
    }

    public String generate(Message.Mutable[] messages) {
        try {
            return getObjectMapper().writeValueAsString(messages);
        } catch (IOException x) {
            throw new RuntimeException(x);
        }
    }

    public JSONContext.Parser getParser() {
        return new ObjectMapperParser();
    }

    public JSONContext.Generator getGenerator() {
        return new ObjectMapperGenerator();
    }

    private class ObjectMapperParser implements JSONContext.Parser {
        public <T> T parse(Reader reader, Class<T> type) throws ParseException {
            try {
                return getObjectMapper().readValue(reader, type);
            } catch (IOException x) {
                throw (ParseException) new ParseException("", -1).initCause(x);
            }
        }
    }

    private class ObjectMapperGenerator implements JSONContext.Generator {
        public String generate(Object object) {
            try {
                return getObjectMapper().writeValueAsString(object);
            } catch (IOException x) {
                throw new RuntimeException(x);
            }
        }
    }
}
