package com.example.app.util;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import javax.inject.Inject;

import flow.StateParceler;

/**
 * Created by yinzara on 8/12/15.
 */
public class JacksonParceler implements StateParceler {

    protected final ObjectMapper objectMapper;

    @Inject
    public JacksonParceler(final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Parcelable wrap(Object instance) {
        return encode(instance);
    }

    @Override
    public Object unwrap(Parcelable parcelable) {
        return decode((Wrapper) parcelable);
    }

    private Wrapper encode(final Object instance) {
        try {
            return new Wrapper(instance.getClass(), objectMapper.writeValueAsString(instance));
        } catch (IOException exp) {
            throw new RuntimeException(exp);
        }
    }

    private Object decode(Wrapper wrapper) {
        try {
            return objectMapper.readValue(wrapper.json, wrapper.type);
        } catch (IOException exp) {
            throw new RuntimeException(exp);
        }
    }

    private static class Wrapper implements Parcelable {
        final Class<?> type;
        final String json;


        Wrapper(Class<?> type, String json) {
            this.json = json;
            this.type = type;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            out.writeString(type.getName());
            out.writeString(json);
        }

        public static final Parcelable.Creator<Wrapper> CREATOR = new Parcelable.Creator<Wrapper>() {
            @Override
            public Wrapper createFromParcel(Parcel in) {
                try {
                    final String typeName = in.readString();
                    final Class<?> type = Class.forName(typeName);
                    final String json = in.readString();
                    return new Wrapper(type, json);
                } catch (final ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public Wrapper[] newArray(int size) {
                return new Wrapper[size];
            }
        };
    }
}
