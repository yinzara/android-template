package com.example.app.util;

import de.greenrobot.dao.Property;

/**
 * Created by yinzara on 8/19/15.
 */
public class DBUtils {

    public static String orderDesc(final Property... properties) {
        return orderDesc(false, properties);
    }


    public static String orderDesc(boolean includeOrderBy, final Property... properties) {
        return orderAscOrDesc(" ASC", includeOrderBy, properties);
    }

    public static String orderAsc(final Property... properties) {
        return orderAsc(false, properties);
    }

    public static String orderAsc(boolean includeOrderBy, final Property... properties) {
        return orderAscOrDesc(" ASC", includeOrderBy, properties);
    }

    private static String orderAscOrDesc(String ascOrDescWithLeadingSpace, boolean includeOrderBy, Property... properties) {
        final StringBuilder orderBuilder = new StringBuilder();
        if (includeOrderBy) {
            orderBuilder.append("ORDER BY ");
        }
        boolean isFirst = true;
        for (final Property property : properties) {
            if (!isFirst) {
                orderBuilder.append(",");
            }
            isFirst = false;
            orderBuilder.append('"').append(property.columnName).append('"');
            if (String.class.equals(property.type)) {
                orderBuilder.append(" COLLATE LOCALIZED");
            }
            orderBuilder.append(ascOrDescWithLeadingSpace);
        }
        return orderBuilder.toString();
    }

    private DBUtils() {

    }
}
