package com.example.app.mediator;

import android.net.Uri;
import android.support.annotation.RawRes;

import com.example.app.R;
import com.example.app.dagger.component.MediatorComponent;
import com.example.app.mediator.base.BaseMediator;
import com.example.app.service.AudioService_;

import org.androidannotations.annotations.EBean;

/**
 * Created by yinzara on 8/17/15.
 */
@EBean(scope=EBean.Scope.Singleton)
public class AudioMediator extends BaseMediator {

    @Override
    protected void doInject(MediatorComponent injector) {
        injector.inject(this);
    }

    public void playSoundResource(@RawRes int resourceId) {
        AudioService_.intent(context).playAudioResource(resourceId).start();
    }

    public void playAudio(Uri uri) {
        AudioService_.intent(context).playAudio(uri).start();
    }

    public void pauseAudio() {
        AudioService_.intent(context).pauseAudio().start();
    }

    public void resumeAudio() {
        AudioService_.intent(context).resumeAudio().start();
    }

    /** Specific application sound play methods **/

    public void bell() {
        playSoundResource(R.raw.bell);
    }

    public void fig() {
        playSoundResource(R.raw.fig);
    }

    public void jublilation() {
        playSoundResource(R.raw.jubilation);
    }

    public void metro() {
        playSoundResource(R.raw.metro);
    }

    public void offair() {
        playSoundResource(R.raw.offair);
    }

    public void solemn() {
        playSoundResource(R.raw.solemn);
    }

    public void sub() {
        playSoundResource(R.raw.sub);
    }
}
