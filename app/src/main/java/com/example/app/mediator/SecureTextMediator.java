package com.example.app.mediator;

import android.util.Log;

import com.example.app.dagger.component.MediatorComponent;
import com.example.app.dao.SecureTextDao;
import com.example.app.entity.SecureText;
import com.example.app.event.EntityEvent;
import com.example.app.event.global.entity.SecureTextEvent;
import com.example.app.event.global.entity.base.BaseEntityEvent;
import com.example.app.event.local.BadgeCountUpdatedEvent;
import com.example.app.mediator.base.BaseMediator;
import com.example.app.rest.SecureTextResource;
import com.example.app.rest.dto.BadgeMessageCountResponse;
import com.example.app.service.SecureTextService_;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.springframework.web.client.RestClientException;

import javax.inject.Inject;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by yinzara on 8/15/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class SecureTextMediator extends BaseMediator {

    private static final String TAG = "SecTxtBS";

    @Inject
    SecureTextResource secureTextResource;

    @Inject
    SecureTextDao secureTextDao;

    @Bean
    ConnectivityMediator connectivityMediator;

    @Background
    public void queryUnreadCount() {
        try {
            final BadgeMessageCountResponse count = secureTextResource.getUnreadCount();
            if (count.getBadge() != null && count.getBadge() > 0) {
                ShortcutBadger.with(context).count(count.getBadge());
            } else {
                ShortcutBadger.with(context).remove();
            }
            publish(new BadgeCountUpdatedEvent(count.getBadge() != null ? count.getBadge() : 0));
        } catch (RestClientException exp) {
            Log.e(TAG, "Unable to retrieve setMessage count", exp);
        }
    }

    public void updateSecureTexts() {
        SecureTextService_
                .intent(context)
                .loadMostRecentSecureTexts()
                .start();
    }

    @Background
    public void deleteSecureText(long secureTextId) {
        final SecureText entity = secureTextDao.load(secureTextId);
        final BaseEntityEvent<SecureText> event = new SecureTextEvent().setType(EntityEvent.Type.DELETE).setEntity(entity).setEntityId(secureTextId);
        if (entity != null && !entity.isDeleted()) {
            if (connectivityMediator.isConnectedToNetwork()) {
                doRemote(event, false, () -> {
                    secureTextResource.deleteSecureText(secureTextId);
                    entity.delete();
                });
            } else {
                event.setFailureReason(EntityEvent.FailureReason.NO_NETWORK);
            }
            //If we failed to delete it from the remote server, just set it as deleted locally then we can update on server later through sync process
            if (!event.isSuccess()) {
                entity.setDeleted(true);
                entity.update();
                event.setFailureReason(null);
            }
            SecureTextService_
                    .intent(context)
                    .deleteConversationIfEmpty(entity.getThreadId())
                    .start();
        } else {
            event.setFailureReason(EntityEvent.FailureReason.NOT_FOUND);
        }

        publish(event);
    }

    @Override
    protected void doInject(MediatorComponent injector) {
        injector.inject(this);
    }
}
