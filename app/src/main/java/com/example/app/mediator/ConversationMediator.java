package com.example.app.mediator;

import com.example.app.dagger.component.MediatorComponent;
import com.example.app.mediator.base.BaseMediator;

import org.androidannotations.annotations.EBean;

/**
 * Created by yinzara on 8/15/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class ConversationMediator extends BaseMediator {

    @Override
    protected void doInject(MediatorComponent injector) {
        injector.inject(this);
    }


}
