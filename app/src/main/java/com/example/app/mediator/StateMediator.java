package com.example.app.mediator;

/**
 * Created by yinzara on 8/20/15.
 */

import com.example.app.dagger.component.MediatorComponent;
import com.example.app.mediator.base.BaseMediator;
import com.example.app.state.AppPreferences_;
import com.example.app.state.DataState_;
import com.example.app.state.LoginState_;
import com.example.app.state.UserPreferences_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import lombok.Getter;

@EBean(scope = EBean.Scope.Singleton)
public class StateMediator extends BaseMediator {


    @Pref
    @Getter
    protected LoginState_ loginState;

    @Pref
    @Getter
    protected UserPreferences_ prefs;

    @Pref
    @Getter
    protected DataState_ dataState;

    @Pref
    @Getter
    protected AppPreferences_ appPrefs;

    @Override
    protected void doInject(MediatorComponent injector) {
        injector.inject(this);
    }
}
