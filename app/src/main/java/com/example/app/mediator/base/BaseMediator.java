package com.example.app.mediator.base;

import android.content.Context;
import android.os.Parcelable;
import android.util.Log;

import com.squareup.otto.Bus;
import com.example.app.VSTApplication;
import com.example.app.dagger.component.MediatorComponent;
import com.example.app.dao.DaoSession;
import de.greenrobot.dao.HasId;
import com.example.app.event.EntityEvent;
import com.example.app.event.RemoteEvent;
import com.example.app.event.global.entity.base.BaseEntityEvent;
import com.example.app.event.local.base.LocalRemoteEvent;
import com.example.app.rest.dto.BaseResponse;
import com.example.app.service.base.BaseService;
import com.example.app.state.DataState_;
import com.example.app.state.LoginState_;
import com.example.app.state.UserPreferences_;

import net.sqlcipher.Cursor;
import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.util.concurrent.Callable;

import javax.inject.Inject;

/**
 * This is the base class for mediators provided by the application. <br/>
 * These are singleton beans that interact between the services and the database. <br/>
 * These classes generally cause SHORT RUNNING operations that may access the database or network in a limited fashion<br/>
 * If a LONG RUNNING operation is required, create a class that extends BaseService instead<br/>
 * <p>
 * They are usually called by the views but not always.<br/>
 * To create a new mediator class:
 * <ol>
 *     <li>Create a class in this package that extends from BaseMediator named 'somename'Mediator (now called 'NewClassMediator' for the rest of this example)</li>
 *     <li>Add the "@EBean(scope = EBean.Scope.Singleton)" annotation to NewClassMediator</li>
 *     <li>Override the "doInject" method and call "injector.inject(this);</li>
 *     <li>Edit the MediatorComponent class and add a "void inject(NewClassMediator service);" method on the interface</li>
 * </ol>
 * You can now @Inject fields or use any AndroidAnnotations injections in NewClassMediator<br/>
 * See LoginMediator for an example<br/>
 * <p>
 * Generally these classes should all have methods with 'void' return types and operate asynchronously (just annotate the method with @Background)<br/>
 * They usually will publish event(s) (from package com.example.app.event) on the bus when the finish their work<br/>
 * When creating new events, if they are as a result of a web service call, extend 'LocalRemoteEvent'<br/>
 * If a RestClientException exception occurs, pass the 'LocalRemoteEvent' object and the exception to the {@link #handleException(RemoteEvent, Exception)} method below<br/>
 * <p>
 * <p>
 * To use one of these classes, it can be injected into any @EBean, @EViewGroup, or @EService using the @Bean annotation (instead of @Inject)<br/>
 * This means you can use these classes in a Presenter, screen View (with no presenter), Service, or other Mediator
 * </p>
 * Created by yinzara on 8/14/15.
 *
 * @see com.example.app.dagger.component.MediatorComponent
 * @see BaseService
 * @see LocalRemoteEvent
 * @see com.example.app.view.base.presenter.ScreenPresenter;
 * @see com.example.app.view.base;

 */
@EBean(scope = EBean.Scope.Singleton)
public abstract class BaseMediator {

    private static final String TAG = BaseMediator.class.getSimpleName();

    @Inject
    Bus bus;

    @Pref
    protected UserPreferences_ prefs;

    @Pref
    protected LoginState_ loginState;

    @Pref
    protected DataState_ dataState;

    @RootContext
    protected Context context;

    @Inject
    protected DaoSession session;

    @AfterInject
    protected final void doInject() {
        doInject(VSTApplication.injector(context).mediator());
        bus.register(this);
    }

    @UiThread
    public void publish(Object event) {
        bus.post(event);
    }

    protected abstract void doInject(MediatorComponent injector);

    /**
     * For any exceptions in web services, the associated exception should be passed here
     *
     * @param event
     * @param exp
     */
    public static void handleException(RemoteEvent event, Exception exp) {
        if (exp instanceof HttpStatusCodeException) {
            final HttpStatusCodeException sce = ((HttpStatusCodeException) exp);
            event.setStatusCode(sce.getStatusCode().value());
            final String responseBody = sce.getResponseBodyAsString();
            try {
                JSONObject obj = new JSONObject(responseBody);
                if (obj.has("response")) {
                    obj = (JSONObject) obj.get("response");
                } else if (obj.has("resp")) {
                    obj = (JSONObject) obj.get("resp");
                }

                if (obj.has("error")) {
                    event.setMessage(obj.getString("error"));
                } else if (obj.has("err")) {
                    event.setMessage(obj.getString("err"));
                } else if (obj.has("setMessage")) {
                    event.setMessage(obj.getString("setMessage"));
                } else if (obj.has("msg")) {
                    event.setMessage(obj.getString("msg"));
                }
            } catch (JSONException jsonExp) {
                event.setMessage(responseBody);
                Log.e(TAG, "Unable to decode error response as JSON: " + responseBody, jsonExp);
            }
            Log.i(TAG, "HTTP failed (" + sce.getStatusCode().name() + ") remote event " + event.getClass().getSimpleName() + "   |   " + exp.getMessage());
        } else {
            event.setMessage(exp.getMessage());
            Log.i(TAG, "Failed HTTP event " + event.getClass().getSimpleName(), exp);
        }

    }

    public interface RemoteOperation<T> {
        void doRemote(T event) throws RestClientException;
    }

    public interface RemoteReturnOperation<T, J> {
        T doRemote(J event) throws RestClientException;
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable> void doRemote(EventType event, RemoteOperation<EventType> op) {
        doRemote(event, true, op);
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable> void doRemote(EventType event, Runnable op) {
        doRemote(event, true, op);
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable, J> J doRemote(EventType event, RemoteReturnOperation<J, EventType> op) {
        return doRemote(event, true, op);
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable, J> J doRemote(EventType event, Callable<J> op) {
        return doRemote(event, true, op);
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable> void doRemote(EventType event, boolean publish, RemoteOperation<EventType> op) {
        try {
            op.doRemote(event);
        } catch (RestClientException exp) {
            Log.e(TAG, "Exception during web service call", exp);
            event.setFailureReason(EntityEvent.FailureReason.REMOTE_ERROR);
        }
        if (publish) {
            publish(event);
        }
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable> void doRemote(EventType event, boolean publish, Runnable op) {
        try {
            op.run();
        } catch (RestClientException exp) {
            Log.e(TAG, "Exception during web service call", exp);
            event.setFailureReason(EntityEvent.FailureReason.REMOTE_ERROR);
        }
        if (publish) {
            publish(event);
        }
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable, J> J doRemote(EventType event, boolean publish, RemoteReturnOperation<J, EventType> op) {
        J val = null;
        try {
            val = op.doRemote(event);
        } catch (RestClientException exp) {
            Log.e(TAG, "Exception during web service call", exp);
            event.setFailureReason(EntityEvent.FailureReason.REMOTE_ERROR);
        }
        if (publish) {
            publish(event);
        }
        return val;
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable, J> J doRemote(EventType event, boolean publish, Callable<J> op) {
        J val = null;
        try {
            val = op.call();
        } catch (Exception exp) {
            Log.e(TAG, "Exception during web service call", exp);
            event.setFailureReason(EntityEvent.FailureReason.REMOTE_ERROR);
        }
        if (publish) {
            publish(event);
        }
        return val;
    }

    protected <T extends RemoteEvent> void doRemote(T event, RemoteOperation<T> op) {
        try {
            op.doRemote(event);
        } catch (RestClientException exp) {
            handleException(event, exp);
        }
        publish(event);
    }

    protected <T extends RemoteEvent> void doRemote(T event, Runnable op) {
        try {
            op.run();
        } catch (RestClientException exp) {
            handleException(event, exp);
        }
        publish(event);
    }

    protected <T, J extends RemoteEvent> T doRemote(J event, RemoteReturnOperation<T, J> op) {
        return doRemote(event, true, op);
    }

    protected <T, J extends RemoteEvent> T doRemote(J event, boolean logNonSuccess, RemoteReturnOperation<T, J> op) {
        T val = null;
        try {
            val = op.doRemote(event);
            if (logNonSuccess) {
                if (val instanceof BaseResponse) {
                    final BaseResponse resp = (BaseResponse) val;
                    if (!resp.isSuccess()) {
                        Log.i(TAG, "Web service call failed: " + resp.getError());
                    }
                }
            }
        } catch (Exception exp) {
            handleException(event, exp);
        }
        publish(event);
        return val;
    }

    protected <T, J extends RemoteEvent> T doRemote(J event, Callable<T> op) {
        return doRemote(event, true, op);
    }

    protected <T, J extends RemoteEvent> T doRemote(J event, boolean logNonSuccess, Callable<T> op) {
        T val = null;
        try {
            val = op.call();
            if (logNonSuccess) {
                if (val instanceof BaseResponse) {
                    final BaseResponse resp = (BaseResponse) val;
                    if (!resp.isSuccess()) {
                        Log.i(TAG, "Web service call failed: " + resp.getError());
                    }
                }
            }
        } catch (Exception exp) {
            handleException(event, exp);
        }
        publish(event);
        return val;
    }

    protected void executeRawSql(final String sql) {
        session.getDatabase().execSQL(sql);
    }

    protected void executeRawSql(final String sql, final Object... args) {
        session.getDatabase().execSQL(sql, args);
    }

    /**
     * @return the number of rows affected by the last update or delete
     */
    protected long getUpdatedCount() {
        Cursor cursor = null;
        try {
            cursor = session.getDatabase().rawQuery("SELECT changes() AS affected_row_count", null);
            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
                return cursor.getLong(cursor.getColumnIndex("affected_row_count"));
            } else {
                // Some error occurred?
            }
        } catch (SQLException e) {
            // Handle exception here.
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return 0;
    }

    protected void doInTransaction(Runnable op) {
        SQLiteDatabase db = session.getDatabase();
        db.beginTransaction();
        try {
            op.run();
            db.setTransactionSuccessful();
        } catch (Exception ex) {
            Log.e(TAG, "Exception during transaction", ex);
        } finally {
            db.endTransaction();
        }
    }

    protected <T> T doInTransaction(Callable<T> op) {
        SQLiteDatabase db = session.getDatabase();
        db.beginTransaction();
        T val = null;
        try {
            val = op.call();
            db.setTransactionSuccessful();
        } catch (Exception ex) {
            Log.e(TAG, "Exception during transaction", ex);
        } finally {
            db.endTransaction();
        }
        return val;
    }
}
