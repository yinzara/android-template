package com.example.app.mediator;

import android.content.Intent;
import android.util.Log;

import com.example.app.R;
import com.example.app.dagger.component.MediatorComponent;
import com.example.app.dao.DaoMaster;
import com.example.app.event.global.remote.LoginEvent;
import com.example.app.mediator.base.BaseMediator;
import com.example.app.rest.UserResource;
import com.example.app.rest.UserResource_;
import com.example.app.rest.dto.LoginResponse;
import com.example.app.service.CometDService;
import com.example.app.state.DataState_;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.res.StringRes;
import org.androidannotations.annotations.sharedpreferences.Pref;

import javax.inject.Inject;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by yinzara on 8/14/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class LoginMediator extends BaseMediator {

    private static final String TAG = "LoginMediator";

    @Inject
    UserResource userResource;

    @StringRes(R.string.login_success)
    String loginSuccessRes;

    @Bean
    SecureTextMediator secureTextMediator;

    @Background
    public void doLogin(final String username, final String password) {
        doRemote(new LoginEvent(), (LoginEvent event) -> {
            final LoginResponse resp = userResource.login(username, password);
            if (resp.isSuccess()) {
                event.setMessage(loginSuccessRes);
                processLoginInfo(resp);
                makeInitialServiceCalls();
            }
        });
    }

    private void processLoginInfo(final LoginResponse resp) {
        loginState.edit()
                .staffId().put(resp.getUser().getStaffId())
                .token().put(resp.getToken())
                .tokenExpiration().put(resp.getTokenExpiration().getMillis())
                .apply();
    }

    private void makeInitialServiceCalls() {
        secureTextMediator.queryUnreadCount();
        context.startService(new Intent(context, CometDService.class));
    }

    public boolean isLoggedIn() {
        final long tokenExpiration = loginState.tokenExpiration().get();
        return tokenExpiration > 0 && tokenExpiration > System.currentTimeMillis();
    }

    public void logout() {
        context.stopService(new Intent(context, CometDService.class));

        //Now we do the work of clearing all the data from the devices
        loginState.clear();
        dataState.clear();

        DaoMaster.dropAllTables(session.getDatabase(), true);
        DaoMaster.createAllTables(session.getDatabase(), false);

        ShortcutBadger.with(context).remove();
    }

    @Override
    protected void doInject(MediatorComponent injector) {
        injector.inject(this);
    }
}
