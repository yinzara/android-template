package com.example.app.mediator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.*;
import android.util.Log;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.example.app.BuildConfig;
import com.example.app.VSTApplication;
import com.example.app.event.global.base.BaseGlobalEvent;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import javax.inject.Inject;

/**
 * Created by yinzara on 8/16/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class GlobalEventMediator {

    private static final String TAG = "GEM";

    @RootContext
    Context context;

    @Inject
    Bus bus;

    public static String GLOBAL_EVENT_BROADCAST_NAME = GlobalEventMediator.class.getSimpleName() + ".EVENT";

    private static final String EVENT_EXTRA = GlobalEventMediator.class.getSimpleName() + ".EVENT_EXTRA";

    public static class GlobalEventReceiver extends BroadcastReceiver {

        private final Bus bus;

        public GlobalEventReceiver(final Bus bus) {
            this.bus = bus;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            final BaseGlobalEvent event = intent.getParcelableExtra(EVENT_EXTRA);
            if (event.isFromOtherProcess()) {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Receiving event " + event.getClass().getSimpleName() + " in PID" + android.os.Process.myPid());
                }
                bus.post(event);
            }
        }
    }

    @AfterInject
    void setupBroadcastReceiver() {
        VSTApplication.injector(context).mediator().inject(this);
        context.registerReceiver(new GlobalEventReceiver(bus), new IntentFilter(GLOBAL_EVENT_BROADCAST_NAME));
    }


    private void publishGlobal(final BaseGlobalEvent event) {
        final Intent intent = new Intent(GLOBAL_EVENT_BROADCAST_NAME);
        intent.putExtra(EVENT_EXTRA, event);
        context.sendBroadcast(intent);
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Broadcasting event " + event.getClass().getSimpleName() + " from PID" + android.os.Process.myPid());
        }
    }

    @Subscribe
    public void onGlobalEvent(BaseGlobalEvent event) {
        if (!event.isFromOtherProcess()) {
            publishGlobal(event);
        }
    }
}
