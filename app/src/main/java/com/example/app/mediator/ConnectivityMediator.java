package com.example.app.mediator;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.app.dagger.component.MediatorComponent;
import com.example.app.mediator.base.BaseMediator;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.SystemService;

/**
 * Created by yinzara on 8/15/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class ConnectivityMediator extends BaseMediator {

    @SystemService
    ConnectivityManager connectivityManager;

    @Override
    protected void doInject(MediatorComponent injector) {
        injector.inject(this);
    }

    public boolean isConnectedToNetwork() {
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public boolean isOnWifi() {
        return connectivityManager.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;
    }
}
