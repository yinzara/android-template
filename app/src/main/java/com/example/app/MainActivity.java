/*
 * Copyright 2013 Square Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.app;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.StringRes;
import android.view.Menu;
import android.view.MenuItem;

import com.example.app.mediator.ConnectivityMediator;
import com.example.app.mediator.GlobalEventMediator;
import com.example.app.mediator.LoginMediator;
import com.example.app.service.CometDDisconnectBroadcastReceiver;
import com.example.app.service.CometDService;
import com.example.app.view.app.login.LoginScreen;
import com.example.app.view.base.pathview.HandlesBack;
import com.example.app.view.app.prompt.PromptModal;
import com.example.app.view.app.splash.SplashScreen;
import com.example.app.view.base.TabletMasterDetailRoot;
import com.example.app.view.base.pathview.Title;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.SystemService;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import flow.Flow;
import flow.FlowDelegate;
import flow.History;
import flow.StateParceler;
import flow.path.Path;
import flow.path.PathContainerView;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

import static flow.Flow.Traversal;
import static flow.Flow.TraversalCallback;

@EActivity
public class MainActivity extends Activity implements Flow.Dispatcher {

    public static long COMETD_SHUTDOWN_DELAY = 60 * 1000L * 5;

    private MortarScope activityScope;

    @Inject
    protected StateParceler parceler;

    @NonConfigurationInstance
    protected FlowDelegate.NonConfigurationInstance nonConfig;

    @Bean
    protected GlobalEventMediator eventMediator;

    @Bean
    protected ConnectivityMediator connectivityMediator;

    @Bean
    LoginMediator loginMediator;

    @SystemService
    AlarmManager alarmManager;

    private PathContainerView container;

    private HandlesBack containerAsBackTarget;

    private FlowDelegate flowSupport;

    private PendingIntent stopCometDPendingIntent;


    /**
     * Pay attention to the {@link #setContentView} call here. It's creating a responsive layout
     * for us.
     * <p>
     * Notice that the app has two root_layout files. The main one, in {@code res/layout} is used by
     * mobile devices and by tablets in portrait orientation. It holds a generic {@link
     * com.example.app.view.base.pathview.FramePathContainerView}.
     * <p>
     * The interesting one, loaded by tablets in landscape mode, is {@code res/layout-sw600dp-land}.
     * It loads a {@link TabletMasterDetailRoot}, view a master list on the
     * left and a detail view on the right.
     * <p>
     * But this master activity knows nothing about those two view types. It only requires that
     * the view loaded by {@code root_layout.xml} implements the {@link PathContainerView} interface,
     * to render whatever is appropriate for the screens received from {@link Flow} via
     * {@link #dispatch}.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.root_layout);

        VSTApplication.injector(this).inject(this);

        stopCometDPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(CometDDisconnectBroadcastReceiver.STOP_SERVICE_IF_IN_BACKGROUND_ACTION), 0);
//
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(false);

        if (loginMediator.isLoggedIn()) {
            startService(new Intent(this, CometDService.class));
            alarmManager.cancel(stopCometDPendingIntent);
        }

        container = (PathContainerView) findViewById(R.id.container);
        containerAsBackTarget = (HandlesBack) container;
        flowSupport = FlowDelegate.onCreate(nonConfig, getIntent(), savedInstanceState, parceler,
                History.single(new SplashScreen()), this);
        nonConfig = flowSupport.onRetainNonConfigurationInstance();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        flowSupport.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        flowSupport.onResume();
    }

    @Override
    protected void onPause() {
        flowSupport.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, COMETD_SHUTDOWN_DELAY + SystemClock.elapsedRealtime(), stopCometDPendingIntent);
    }

    @Override
    public Object getSystemService(String name) {
        Object service = null;
        if (flowSupport != null) {
            service = flowSupport.getSystemService(name);
        }
        if (activityScope == null) {
            activityScope = MortarScope.findChild(getApplicationContext(), "ACTIVITY");
        }

        if (activityScope == null) {
            activityScope = MortarScope.buildChild(getApplicationContext()) //
                    .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                    .build("ACTIVITY");
        }

        return service != null? service : activityScope.hasService(name) ? activityScope.getService(name)
                : super.getSystemService(name);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        flowSupport.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.string.logout)
                .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                .setOnMenuItemClickListener((MenuItem item) -> {
                    loginMediator.logout();
                    Flow.get(MainActivity.this).setHistory(History.single(new LoginScreen()), Flow.Direction.REPLACE);
                    return true;
                });
//        menu.add("Friends")
//                .setShowAsActionFlags(SHOW_AS_ACTION_ALWAYS)
//                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem menuItem) {
//                        Flow.get(MainActivity.this).setHistory(History.emptyBuilder() //
//                                .push(new Paths.RecentConversationsScreen()) //
//                                .push(new Paths.DirectoryListScreen()) //
//                                .build(), FORWARD);
//                        return true;
//                    }
//                });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (containerAsBackTarget.onBackPressed()) return;
        if (flowSupport.onBackPressed()) return;
        if (getIntent() == null || getIntent().getAction().equals("android.intent.action.MAIN")) {
            Flow.get(this).set(PromptModal.confirmExit());
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void dispatch(Traversal traversal, final TraversalCallback callback) {
        Path path = traversal.destination.top();
        final @StringRes Integer titleId = getScreenTitle(path.getClass());
        if (titleId != null && titleId > 0) {
            setTitle(titleId);
        } else {
            setTitle(null);
        }
        ActionBar actionBar = getActionBar();
        boolean canGoBack = traversal.destination.size() > 1;
        actionBar.setDisplayHomeAsUpEnabled(canGoBack);
        actionBar.setHomeButtonEnabled(canGoBack);
        container.dispatch(traversal, new TraversalCallback() {
            @Override
            public void onTraversalCompleted() {
                invalidateOptionsMenu();
                callback.onTraversalCompleted();
            }
        });
    }

    private static final Map<Class<?>, Integer> SCREEN_TITLES = new HashMap<Class<?>, Integer>();

    @StringRes
    private Integer getScreenTitle(final Class<?> clazz) {
        if (SCREEN_TITLES.containsKey(clazz)) {
            return SCREEN_TITLES.get(clazz);
        } else {
            final Title title = clazz.getAnnotation(Title.class);
            SCREEN_TITLES.put(clazz, title != null? title.value():null);
            return title != null? title.value():null;
        }
    }
}
