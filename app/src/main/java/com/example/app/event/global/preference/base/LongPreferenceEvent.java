package com.example.app.event.global.preference.base;

import android.os.Parcel;

/**
 * Event raised when a Double is changed in shared preferences
 * <p>
 * Created by yinzara on 8/16/15.
 */
//Example CREATOR for child classes:
//public static final Parcelable.Creator<MyPreferenceEvent> CREATOR = new LongPreferenceEvent.Creator<MyPreferenceEvent>() {};
public abstract class LongPreferenceEvent extends PrimitivePreferenceEvent<Long> {

    public static <EventType extends LongPreferenceEvent> Creator<EventType> newCreator(Class<EventType> eventType) {
        return new Creator<EventType>(eventType) {};
    }

    public static abstract class Creator<EventType extends LongPreferenceEvent> extends PrimitivePreferenceEvent.Creator<EventType, Long> {

        public Creator(Class<EventType> eventClass) {
            super(eventClass);
        }

        protected Long readValue(Parcel source) {
            return source.readLong();
        }

    };

    public LongPreferenceEvent() { super(); }

    public LongPreferenceEvent(Long oldValue, Long newValue) {
        super(oldValue, newValue);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        if (oldValue != null) {
            dest.writeLong(oldValue);
        }
        if (newValue != null) {
            dest.writeLong(newValue);
        }
    }
}
