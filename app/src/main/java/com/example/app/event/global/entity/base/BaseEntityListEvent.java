package com.example.app.event.global.entity.base;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import de.greenrobot.dao.HasId;
import com.example.app.event.EntityListEvent;
import com.example.app.util.Utils;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by yinzara on 8/16/15.
 */
public abstract class BaseEntityListEvent<EntityType extends HasId & Parcelable> implements EntityListEvent<EntityType>, Parcelable {

    private static final String TAG = "BaseEntityListEvent";

    @Getter @Setter
    private List<Long> inserted = new LinkedList<>();

    @Getter @Setter
    private List<EntityType> updated = new LinkedList<>();

    @Getter @Setter
    private List<Long> deleted = new LinkedList<>();

    private final Class<EntityType> entityClass;

    protected BaseEntityListEvent(Class<EntityType> entityClass) {
        this.entityClass = entityClass;
    }

    @SuppressWarnings({"unchecked","rawtypes"})
    protected static final <EventType extends BaseEntityListEvent<EntityType>,EntityType extends HasId & Parcelable> Parcelable.Creator<EventType> newCreator(final Class<EventType> eventClass, final Class<EntityType> entityClass) {
        return new Parcelable.Creator<EventType>() {

            @Override
            public EventType createFromParcel(Parcel source) {
                try {
                    final EventType event = eventClass.newInstance();

                    event.setInserted(Utils.toList(source.createLongArray()));
                    final Parcelable[] arr = source.readParcelableArray(eventClass.getClassLoader());
                    if (arr != null) {
                        event.setUpdated((List) Arrays.asList(arr));
                    }
                    event.setDeleted(Utils.toList(source.createLongArray()));

                    return event;
                } catch (Exception exp) {
                    final String message = "EntityEvent of type " + eventClass.getName() + " does not a no args constructor. Add the @NoArgsConstructor annotation from lomboc";
                    Log.e(TAG, message);
                    throw new IllegalStateException(message);
                }
            }

            @Override
            public EventType[] newArray(int size) {
                return (EventType[]) Array.newInstance(eventClass, size);
            }
        };
    }


    @Override
    @SuppressWarnings({"unchecked","rawtypes"})
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLongArray(Utils.toArray(inserted));
        dest.writeParcelableArray(updated != null && !updated.isEmpty()? (EntityType[]) Array.newInstance(entityClass, updated.size()) : null, 0);
        dest.writeLongArray(Utils.toArray(deleted));
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
