package com.example.app.event.global.remote;

import android.os.Parcelable;

import com.example.app.event.global.remote.base.GlobalRemoteEvent;

/**
 * This is an example of a simple GlobalRemoteEvent (with no additional properties), refactor to make new simple events
 * Created by yinzara on 8/16/15.
 */
public class ExampleRemoteEvent extends GlobalRemoteEvent {

    public static final Parcelable.Creator<ExampleRemoteEvent> CREATOR = GlobalRemoteEvent.newCreator(ExampleRemoteEvent.class);
}
