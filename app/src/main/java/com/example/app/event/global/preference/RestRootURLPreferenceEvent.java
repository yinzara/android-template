package com.example.app.event.global.preference;

import android.os.Parcelable;

import com.example.app.event.global.preference.base.StringPreferenceEvent;

/**
 * This event is raised whenever the root URL to access restful webservices has changed
 * Created by yinzara on 8/14/15.
 */
public class RestRootURLPreferenceEvent extends StringPreferenceEvent implements Parcelable {

    public static final Parcelable.Creator<RestRootURLPreferenceEvent> CREATOR = StringPreferenceEvent.newCreator(RestRootURLPreferenceEvent.class);

}
