package com.example.app.event.global.preference.base;

import android.os.Parcel;

/**
 * Event raised when a integer is changed in shared preferences
 * <p>
 * Created by yinzara on 8/16/15.
 */
//Example creator for child classes:
////public static final Parcelable.Creator<MyPreferenceEvent> CREATOR = new IntegerPreferenceEvent.Creator<MyPreferenceEvent>() {};
public abstract class IntegerPreferenceEvent extends PrimitivePreferenceEvent<Integer> {

    public static <EventType extends IntegerPreferenceEvent> Creator<EventType> newCreator(final Class<EventType> eventClass) {
        return new Creator<EventType>(eventClass);
    }

    public static class Creator<EventType extends IntegerPreferenceEvent> extends PrimitivePreferenceEvent.Creator<EventType, Integer> {

        public Creator(Class<EventType> eventClass) {
            super(eventClass);
        }

        @Override
        protected Integer readValue(Parcel source) {
            return source.readInt();
        }

    };

    public IntegerPreferenceEvent() { super(); }

    public IntegerPreferenceEvent(Integer oldValue, Integer newValue) {
        super(oldValue, newValue);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        if (oldValue != null) {
            dest.writeInt(oldValue);
        }
        if (newValue != null) {
            dest.writeInt(newValue);
        }
    }
}
