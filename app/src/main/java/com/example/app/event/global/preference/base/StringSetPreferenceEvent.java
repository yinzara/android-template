package com.example.app.event.global.preference.base;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Event raised when a string set is changed in shared preferences
 *
 * Created by yinzara on 8/16/15.
 */
//Example CREATOR for child classes:
//public static final Parcelable.Creator<MyPreferenceEvent> CREATOR = new StringSetPreferenceEvent.Creator<MyPreferenceEvent>() {};
public abstract class StringSetPreferenceEvent extends SharedPreferenceEvent<Set<String>> {

    public static <EventType extends StringSetPreferenceEvent> Parcelable.Creator<EventType> newCreator(final Class<EventType> eventClass) {
        return new Creator<EventType>(eventClass) {};
    }

    public abstract static class Creator<EventType extends StringSetPreferenceEvent> extends SharedPreferenceEvent.Creator<EventType,Set<String>> {

        public Creator(final Class<EventType> eventType) {
            super(eventType);
        }

        @Override
        protected Set<String> readValue(Parcel source) {
            final String[] values = source.createStringArray();
            if (values != null) {
                return new HashSet<String>(Arrays.asList(values));
            } else {
                return null;
            }
        }
    };

    public StringSetPreferenceEvent() { super(); }

    public StringSetPreferenceEvent(Set<String> oldValue, Set<String> newValue) {
        super(oldValue, newValue);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeArray(oldValue != null?oldValue.toArray(new String[oldValue.size()]):null);
        dest.writeArray(newValue != null ? newValue.toArray(new String[newValue.size()]) : null);
    }
}
