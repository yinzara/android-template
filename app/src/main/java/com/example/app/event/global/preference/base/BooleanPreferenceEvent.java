package com.example.app.event.global.preference.base;

import android.os.Parcel;

/**
 * Event raised when a integer is changed in shared preferences
 * <p>
 * Created by yinzara on 8/16/15.
 */
//Example CREATOR for child classes:
//public static final Parcelable.Creator<MyPreferenceEvent> CREATOR = new BooleanPreferenceEvent.Creator<MyPreferenceEvent>() {};
public abstract class BooleanPreferenceEvent extends SharedPreferenceEvent<Boolean> {

    private static final byte BYTE_TRUE = 1;

    private static final byte BYTE_FALSE = 0;

    private static final byte BYTE_NULL = 2;

    public static <EventType extends BooleanPreferenceEvent> Creator<EventType> newCreator(final Class<EventType> eventClass) {
        return new Creator<EventType>(eventClass);
    }

    public static class Creator<EventType extends BooleanPreferenceEvent> extends SharedPreferenceEvent.Creator<EventType, Boolean> {

        public Creator(Class<EventType> eventClass) {
            super(eventClass);
        }

        @Override
        protected Boolean readValue(Parcel source) {
            switch (source.readByte()) {
                case BYTE_TRUE:
                    return Boolean.TRUE;
                case BYTE_FALSE:
                    return Boolean.FALSE;
                default:
                    return null;
            }
        }
    };

    public BooleanPreferenceEvent() { super(); }

    public BooleanPreferenceEvent(Boolean oldValue, Boolean newValue) {
        super(oldValue, newValue);
    }

    private static byte byteValue(final Boolean value) {
        if (value == null) {
            return BYTE_NULL;
        } else if (value.booleanValue()) {
            return BYTE_TRUE;
        } else {
            return BYTE_FALSE;
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte(byteValue(oldValue));
        dest.writeByte(byteValue(newValue));
    }
}
