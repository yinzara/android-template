/**
 * This package contains events that will pass the process boundary.  Because our services are configured to be in separate processes than the main application,
 * this will ensure we can pass events over the bus as each process will have it's own bus.  This is all managed via the GlobalEventMediator<p></p>
 * <p>
 * 1. To make a new event, create class that extends BaseGlobalEvent, GlobalRemoteEvent, or any classes in .preference.base
 * 2. Create a public static final "CREATOR" field that extends from the Creator class of the event superclass (see RestRootURLPreferenceEvent, AlertEvent, ExampleRemoteEvent, ConversationEvent, and ConversationListEvent for examples)
 * 3. If you need custom fields on the event (i.e. not just ones from the superclass), ensure you implement the "Parcelable" interface and create the appropriate methods (see AlertEvent for an example)
 *
 * @see android.os.Parcelable
 * @see com.example.app.event.global.remote.AlertEvent
 * @see com.example.app.event.global.base.BaseGlobalEvent
 * @see com.example.app.event.global.remote.base.GlobalRemoteEvent
 * @see com.example.app.event.global.preference.base;
 * @see com.example.app.event.global.entity.base.BaseEntityEvent;
 * @see com.example.app.event.global.entity.base.BaseEntityListEvent;
 */
package com.example.app.event.global;