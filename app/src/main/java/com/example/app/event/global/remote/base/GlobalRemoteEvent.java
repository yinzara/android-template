package com.example.app.event.global.remote.base;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.app.event.RemoteEvent;
import com.example.app.event.global.base.BaseGlobalEvent;
import com.example.app.event.global.remote.AlertEvent;
import com.example.app.event.global.remote.ExampleRemoteEvent;
import com.example.app.util.Utils;

import java.lang.reflect.Array;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Base abstract class for all events that are published from the service layer that originated with a remote service<br/>
 * These events are used to communicate from the service layer to the controllers in the view tier<br/>
 * See {@see AlertEvent} and {@see ExampleRemoteEvent} for information on how to create new events
 *
 * @see AlertEvent
 * @see ExampleRemoteEvent
 * Created by yinzara on 8/16/15.
 */
public abstract class GlobalRemoteEvent extends BaseGlobalEvent implements RemoteEvent {

    public static <EventType extends GlobalRemoteEvent> Parcelable.Creator<EventType> newCreator(final Class<EventType> eventClass) {
        return new BaseGlobalEvent.Creator<EventType>() {

            @Override
            public EventType[] newArray(int size) {
                return (EventType[])Array.newInstance(eventClass, size);
            }

            @Override
            protected EventType newInstance() {
                try {
                    return eventClass.newInstance();
                } catch (Exception exp) {
                    throw new RuntimeException(eventClass.getName() + " did not have a 0 args constructor");
                }
            }
        };
    }

    public static class Creator<EventType extends GlobalRemoteEvent> extends BaseGlobalEvent.Creator<EventType> {

        public Creator(Class<EventType> eventType) {
            this.eventClass = eventType;
        }
        private Class<EventType> eventClass;

        @Override
        public EventType createFromParcel(Parcel in) {
            final EventType event = super.createFromParcel(in);
            event.setMessage(in.readString());
            event.setStatusCode(in.readInt());
            return event;
        }

        @Override
        protected EventType newInstance() {
            try {
                return eventClass.newInstance();
            } catch (Exception exp) {
                throw new IllegalStateException("Child classes of GlobalRemoteEvent must have a default no args constructor", exp);
            }
        }

        @Override
        public EventType[] newArray(int size) {
            return (EventType[]) Array.newInstance(eventClass, size);
        }
    }

    private String message;

    private int statusCode;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeInt(statusCode);
    }

    @Override
    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean isSuccess() {
        return statusCode == 0 || statusCode % 100 == 2;
    }
}
