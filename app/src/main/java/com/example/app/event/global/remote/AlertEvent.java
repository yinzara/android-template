package com.example.app.event.global.remote;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.app.event.global.remote.base.GlobalRemoteEvent;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * This is an example of a GlobalRemoteEvent that has additional fields to parcel
 * <p></p>
 * Refactor copy this to make your own event with additional properties
 *
 * Created by yinzara on 8/16/15.
 */
public class AlertEvent extends GlobalRemoteEvent implements Parcelable {

    public static final Parcelable.Creator<AlertEvent> CREATOR = new GlobalRemoteEvent.Creator<AlertEvent>(AlertEvent.class) {

        @Override
        public AlertEvent createFromParcel(Parcel in) {
            final AlertEvent event = super.createFromParcel(in);
            event.priority = in.readInt();
            return event;
        }

    };

    @Getter
    @Setter
    private int priority;

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(priority);
    }
}
