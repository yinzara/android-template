package com.example.app.event.local;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by yinzara on 8/15/15.
 */
@NoArgsConstructor
@AllArgsConstructor
public class BadgeCountUpdatedEvent {

    @Getter
    @Setter
    private int count;
}
