package com.example.app.event.global.preference.base;

import android.os.Parcel;

/**
 * Created by yinzara on 8/16/15.
 */
public abstract class PrimitivePreferenceEvent<ValueType> extends SharedPreferenceEvent<ValueType> {

    protected static final byte OLD_HAS_VALUE = 1;

    protected static final byte NEW_HAS_VALUE = 2;

    protected static final byte NONE_HAVE_VALUES = 0;

    protected static final byte BOTH_HAVE_VALUES = OLD_HAS_VALUE & NEW_HAS_VALUE;

    protected abstract static class Creator<EventType extends SharedPreferenceEvent<ValueType>, ValueType> extends SharedPreferenceEvent.Creator<EventType, ValueType> {

        public Creator(final Class<EventType> eventType) {
            super(eventType);
        }

        @Override
        public EventType createFromParcel(Parcel source) {
            final EventType event = super.createFromParcel(source);
            final byte nullCheck = source.readByte();
            if ((nullCheck & OLD_HAS_VALUE) > 0) {
                event.oldValue = readValue(source);
            }
            if ((nullCheck & NEW_HAS_VALUE) > 0) {
                event.newValue = readValue(source);
            }
            return event;
        }
    }

    public PrimitivePreferenceEvent() { super(); }

    public PrimitivePreferenceEvent(ValueType oldValue, ValueType newValue) {
        super(oldValue, newValue);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        byte nullCheck = oldValue != null ? OLD_HAS_VALUE : NONE_HAVE_VALUES;
        nullCheck |= newValue != null ? NEW_HAS_VALUE : NONE_HAVE_VALUES;
        dest.writeByte(nullCheck);
    }
}
