package com.example.app.event.global.base;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Getter;
import lombok.Setter;

/**
 * Base class for all global events (i.e. events that propagate across process boundaries)<br/>
 * This includes:
 * <ol>
 *     <li>All events that notify changes in shared preferences</li>
 *     <li>All events sent from a service to the mediator tier</li>
 *     <li>All entity change events (usually only broadcast from the service tier)</li>
 * </ol>
 *
 * Created by yinzara on 8/16/15.
 */
public abstract class BaseGlobalEvent implements Parcelable {

    public abstract static class Creator<T extends BaseGlobalEvent> implements Parcelable.Creator<T> {

        @Override
        public T createFromParcel(Parcel source) {
            T value = newInstance();
            value.pid = source.readInt();
            return value;
        }

        protected abstract T newInstance();
    }
    @Getter
    @Setter
    int pid = android.os.Process.myPid();

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(pid);
    }

    public boolean isFromOtherProcess() {
        return pid != android.os.Process.myPid();
    }
}
