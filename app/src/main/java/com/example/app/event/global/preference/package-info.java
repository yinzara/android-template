/**
 * This package contains events related to SharedPreference(s) changing<br/>
 *
 * To create a new event, extend from one of the classes in the 'com.example.app.event.global.preference' package and add the CREATOR line
 *
 * @see com.example.app.event.global.preference.RestRootURLPreferenceEvent
 */
package com.example.app.event.global.preference;