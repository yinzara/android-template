package com.example.app.event.global.preference.base;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.app.event.global.base.BaseGlobalEvent;
import com.example.app.util.Utils;

import java.lang.reflect.Array;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by yinzara on 8/16/15.
 */
public abstract class SharedPreferenceEvent<ValueType> extends BaseGlobalEvent implements Parcelable {

    protected abstract static class Creator<EventType extends SharedPreferenceEvent<ValueType>, ValueType> extends BaseGlobalEvent.Creator<EventType> {

        private Class<EventType> eventClass;

        public Creator(final Class<EventType> eventClass) {
            this.eventClass = eventClass;
        }

        @Override
        public EventType createFromParcel(Parcel source) {
            final EventType event = super.createFromParcel(source);
            event.oldValue = readValue(source);
            event.newValue = readValue(source);
            return event;
        }

        @Override
        public EventType[] newArray(int size) {
            return (EventType[]) Array.newInstance(eventClass, size);
        }

        @Override
        protected EventType newInstance() {
            try {
                return eventClass.newInstance();
            } catch (Exception exp) {
                throw new RuntimeException("Event class: " + eventClass + " did not have a default no args constructor. Add @NoArgsConstructor annotation to class");
            }
        }

        protected abstract ValueType readValue(Parcel source);

    }

    @Getter
    @Setter
    ValueType oldValue;

    @Getter
    @Setter
    ValueType newValue;

    public SharedPreferenceEvent() {
        super();
    }

    public SharedPreferenceEvent(final ValueType oldValue, final ValueType newValue) {
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }
}
