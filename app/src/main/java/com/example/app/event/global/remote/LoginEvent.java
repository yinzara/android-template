package com.example.app.event.global.remote;

import android.os.Parcelable;

import com.example.app.event.global.remote.base.GlobalRemoteEvent;
import com.example.app.event.local.base.LocalRemoteEvent;

import lombok.NoArgsConstructor;

/**
 * This event is sent in response to an authentication attempt on the VST server
 *
 * Created by yinzara on 8/14/15.
 */
@NoArgsConstructor
public class LoginEvent extends GlobalRemoteEvent implements Parcelable {

    public static final Parcelable.Creator<LoginEvent> CREATOR = GlobalRemoteEvent.newCreator(LoginEvent.class);

}
