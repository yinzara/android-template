package com.example.app.event;

/**
 * Base class for all events produced because of a request to an external service
 *
 * Created by yinzara on 8/14/15.
 */
public interface RemoteEvent {

    String getMessage();

    void setMessage(String message);

    int getStatusCode();

    void setStatusCode(int statusCode);

    boolean isSuccess();
}
