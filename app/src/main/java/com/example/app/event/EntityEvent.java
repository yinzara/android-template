package com.example.app.event;

/**
 * Base class for all events related to individual entities being inserted, updated or deleted
 *
 * Created by yinzara on 8/13/15.
 */
public interface EntityEvent<EntityType> {

    enum Type {
        INSERT, UPDATE, DELETE
    }

    enum FailureReason {
        REMOTE_ERROR, NOT_FOUND, NO_NETWORK
    }


    Type getType();

    EntityType getEntity();

    boolean isSuccess();

    FailureReason getFailureReason();

}
