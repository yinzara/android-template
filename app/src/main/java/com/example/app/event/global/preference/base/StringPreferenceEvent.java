package com.example.app.event.global.preference.base;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Set;

/**
 * Event raised when a string is changed in shared preferences
 * <p>
 * Created by yinzara on 8/16/15.
 */
//public static final Parcelable.Creator<MyPreferenceEvent> CREATOR = new StringPreferenceEvent.Creator<MyPreferenceEvent>() {};
public abstract class StringPreferenceEvent extends SharedPreferenceEvent<String> implements Parcelable {

    public static <V extends StringPreferenceEvent> Parcelable.Creator<V> newCreator(Class<V> eventClass) {
        return new Creator<V>(eventClass) {};
    }

    public abstract static class Creator<V extends StringPreferenceEvent> extends SharedPreferenceEvent.Creator<V, String> {

        public Creator(Class<V> clazz) {
            super(clazz);
        }
        @Override
        protected String readValue(Parcel source) {
            return source.readString();
        }
    }

    public StringPreferenceEvent() { super(); }

    public StringPreferenceEvent(String oldValue, String newValue) {
        super(oldValue, newValue);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(oldValue);
        dest.writeString(newValue);
    }
}
