/**
 * <p>This package contains all events used to publish over the Otto event bus</p>
 * <p>There are two primary types of events: local and global (corresponding to the subpackages in this package)</p>
 * <p>
 * Local events are those that remain completely within the same tier of the application they are published in (i.e. view or service tier).<br/>
 * They can be published from Mediator(s) and received by View(s) [only Title views without a Presenter] or Presenter(s) or other Mediator(s).<br/>
 * They can also be published from Services(s) and received by other Services(s)<br/>
 * They are just simple pojos with no other requirement.
 * </p>
 * <p>
 * Global events are those that cross the boundary between the application tiers (i.e. view or service tier).<br/>
 * They can be published from Service(s), Mediator(s) or Presenter(s).<br/>
 * Because they cross process boundaries they MUST be Parcelable<br/>
 * There are base classes and examples listed in the package info for the global package
 * </p>
 *
 * @see com.example.app.event.global;
 * @see com.example.app.event.local;
 * @see com.squareup.otto.Bus
 * @see com.example.app.mediator.base.BaseMediator ;
 * @see com.example.app.view.base.presenter.ScreenPresenter;
 * @see com.example.app.service.base.BaseService ;
 * @see android.os.Parcelable
 */

package com.example.app.event;