package com.example.app.event.global.preference.base;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Event raised when a Float is changed in shared preferences
 * <p>
 * Created by yinzara on 8/16/15.
 */
//Example CREATOR for child classes
//public static final Parcelable.Creator<MyPreferenceEvent> CREATOR = new FloatPreferenceEvent.Creator<MyPreferenceEvent>() {};
public abstract class FloatPreferenceEvent extends PrimitivePreferenceEvent<Float> {

    public static <EventType extends FloatPreferenceEvent> Parcelable.Creator<EventType> newCreator(final Class<EventType> eventClass) {
        return new Creator<>(eventClass);
    }

    public static class Creator<EventType extends FloatPreferenceEvent> extends PrimitivePreferenceEvent.Creator<EventType, Float> {

        public Creator(final Class<EventType> eventClass) {
            super(eventClass);
        }

        @Override
        protected Float readValue(Parcel source) {
            return source.readFloat();
        }

    };

    public FloatPreferenceEvent() { super(); }

    public FloatPreferenceEvent(Float oldValue, Float newValue) {
        super(oldValue, newValue);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        if (oldValue != null) {
            dest.writeFloat(oldValue);
        }
        if (newValue != null) {
            dest.writeFloat(newValue);
        }
    }
}
