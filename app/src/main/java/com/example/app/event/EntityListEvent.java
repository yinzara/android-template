package com.example.app.event;

import android.os.Parcelable;

import de.greenrobot.dao.HasId;

import java.util.List;

/**
 * Base event for the result from a group of entity operations
 *
 * Created by yinzara on 8/16/15.
 */
public interface EntityListEvent<EntityType extends HasId & Parcelable> {

    List<Long> getInserted();

    List<EntityType> getUpdated();

    List<Long> getDeleted();
}
