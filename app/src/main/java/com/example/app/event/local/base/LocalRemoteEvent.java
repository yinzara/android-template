package com.example.app.event.local.base;

import com.example.app.event.RemoteEvent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by yinzara on 8/16/15.
 */
@AllArgsConstructor
@NoArgsConstructor
public class LocalRemoteEvent implements RemoteEvent {

    private String message;

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    private int statusCode;

    public boolean isSuccess() {
        return statusCode == 0 || statusCode % 100 == 2;
    }
}
