package com.example.app.dagger.component;

import com.example.app.dagger.ViewScoped;
import com.example.app.dagger.module.ViewModule;
import com.example.app.entity.dagger.component.EntityViewComponent;
import com.example.app.view.app.login.LoginView;
import com.example.app.view.app.prompt.PromptView;
import com.example.app.view.app.splash.SplashView;
import com.example.app.view.base.BaseLinearLayout;
import com.example.app.view.base.BaseListView;
import com.example.app.view.base.BaseRelativeLayout;
import com.example.app.view.securetext.conversation.ConversationView;
import com.example.app.view.securetext.message.MessageView;
import com.example.app.view.securetext.recentconvos.RecentConversationsView;

import dagger.Subcomponent;

/**
 * This interface manages Dagger 2 dependency injection for all view scoped objects
 * <p></p>
 * If you would like to @Inject dependencies into a view scoped class (doesn't actually have to extend from View but must be injected into a view):<br/>
 * 1. Add an "inject" method below view the class you would like to inject into<br/>
 * 2. In the class you will need to call VSTApplication.injector(view).inject(objToInjectInto) inside the constructor of the class or during the lifecycle where "view" is View to scope against and 'objToInjectInto' is the object being injected<br/>
 * <p>
 * Note: For the base classes in com.example.app.view.base, if you extend the class, do not do step 2 above, instead override the "doInject" method and call "component.inject(this);". See ConversationListView for example
 * <p>
 * Created by yinzara on 8/13/15.
 */
@ViewScoped
@Subcomponent(
        modules = {ViewModule.class}
)
public interface ViewComponent extends EntityViewComponent {

    ApplicationComponent app();

    void inject(RecentConversationsView view);

    void inject(ConversationView view);

    void inject(LoginView view);

    void inject(PromptView view);

    void inject(SplashView view);

    void inject(MessageView view);

}
