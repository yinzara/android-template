package com.example.app.dagger.component;

import com.example.app.service.AudioService;
import com.example.app.service.CometDService;
import com.example.app.service.base.BaseService;
import com.example.app.service.SecureTextService;

import dagger.Subcomponent;

/**
 * Created by yinzara on 8/16/15.
 */
@Subcomponent
public interface ServiceComponent {

    void inject(SecureTextService service);

    void inject(AudioService audioService);

    void inject(CometDService cometDService);
}
