package com.example.app.dagger.module;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.example.app.dagger.ViewScoped;

import java.lang.ref.WeakReference;

import dagger.Module;
import dagger.Provides;
import mortar.bundler.BundleService;

/**
 * Provies @ViewScoped objects, generally not used
 *
 * Created by yinzara on 8/13/15.
 */
@Module
public class ViewModule {

    private WeakReference<View> view;

    public ViewModule(final View view) {
        this.view = new WeakReference<View>(view);
    }

    @Provides
    Context provideViewContext() {
        final View view = provideView();
        return view != null?view.getContext():null;
    }

    @Provides
    View provideView() {
        return view.get();
    }

    @Provides
    ViewGroup provideViewGroup() {
        if (view.get() instanceof ViewGroup) {
            return (ViewGroup)view.get();
        } else {
            return null;
        }
    }

    @Provides
    BundleService provideBundleService() {
        final View v = view.get();
        if (v != null) {
            return BundleService.getBundleService(view.get().getContext());
        } else {
            return null;
        }
    }

}
