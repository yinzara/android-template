package com.example.app.dagger.module;

import com.example.app.VSTApplication;
import com.example.app.dao.DaoMaster;
import com.example.app.daogenerator.Upgrader;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 *
 * Provides the application database context
 *
 * Created by yinzara on 8/13/15.
 */
@Module
public class DatabaseModule {

    private VSTApplication app;

    public DatabaseModule(VSTApplication app) {
        this.app = app;
    }

    private static final String PASSWORD = "ashf3i2423kjadlf2";

    @Provides
    @Singleton
    SQLiteOpenHelper providerOpenHelper() {
        SQLiteDatabase.loadLibs(app);

        //This will cause the entire database to be dropped and recreated view each app start
        return newDropDbHelper();

        //This will only cause the database tables to be created if they don't exist, must manage other upgrades here
//        return newUpgradeDbHelper()
    }

    private SQLiteOpenHelper newDropDbHelper() {
        return new DaoMaster.DevOpenHelper(app, "vst", null);
    }

    private SQLiteOpenHelper newUpgradeDbHelper() {
        return new DaoMaster.OpenHelper(app, "vst", null) {
            @Override
            public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
                Upgrader.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
                onCreate(sqLiteDatabase);
            }
        };
    }

    @Provides
    @Singleton
    SQLiteDatabase provideDatabase(SQLiteOpenHelper helper) {
        return helper.getWritableDatabase(PASSWORD);
    }

    @Singleton
    @Provides
    DaoMaster provideDaoMaster(SQLiteDatabase db) {
        return new DaoMaster(db);
    }

}
