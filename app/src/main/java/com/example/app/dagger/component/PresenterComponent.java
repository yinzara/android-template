package com.example.app.dagger.component;

import com.example.app.view.app.login.LoginViewPresenter;
import com.example.app.view.securetext.conversation.ConversationPresenter;
import com.example.app.view.securetext.recentconvos.RecentConversationsPresenter;

import dagger.Subcomponent;

/**
 * Created by yinzara on 8/17/15.
 */
@Subcomponent
public interface PresenterComponent {

    ApplicationComponent app();

    void inject(RecentConversationsPresenter presenter);

    void inject(LoginViewPresenter presenter);

    void inject(ConversationPresenter presenter);

}
