package com.example.app.dagger.component;

import com.example.app.mediator.AudioMediator;
import com.example.app.mediator.StateMediator;
import com.example.app.mediator.base.BaseMediator;
import com.example.app.mediator.ConnectivityMediator;
import com.example.app.mediator.ConversationMediator;
import com.example.app.mediator.GlobalEventMediator;
import com.example.app.mediator.LoginMediator;
import com.example.app.mediator.SecureTextMediator;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by yinzara on 8/14/15.
 */
@Singleton
@Subcomponent
public interface MediatorComponent {

    ApplicationComponent app();

    void inject(LoginMediator loginController);

    void inject(ConversationMediator convoController);

    void inject(SecureTextMediator secureTextController);

    void inject(ConnectivityMediator connectivityController);
    
    void inject(GlobalEventMediator eventController);

    void inject(AudioMediator audioController);

    void inject(StateMediator stateMediator);

}
