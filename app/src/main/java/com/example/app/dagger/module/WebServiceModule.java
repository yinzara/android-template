package com.example.app.dagger.module;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.example.app.VSTApplication;
import com.example.app.event.global.preference.RestRootURLPreferenceEvent;
import com.example.app.rest.ConversationResource;
import com.example.app.rest.ConversationResource_;
import com.example.app.rest.SecureTextResource;
import com.example.app.rest.SecureTextResource_;
import com.example.app.rest.UserResource;
import com.example.app.rest.UserResource_;
import com.example.app.state.AppPreferences_;
import com.example.app.state.LoginState_;
import com.example.app.state.UserPreferences_;

import org.androidannotations.api.rest.RestClientRootUrl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 *
 * Module where web service related classes are produced<br/>
 * See package-info.java of com.example.app.rest for more information
 *
 * Created by yinzara on 8/14/15.
 */
@Module
public class WebServiceModule {

    private VSTApplication app;

    private AppPreferences_ prefs;

    private Bus bus;

    private Collection<RestClientRootUrl> clients = Collections.synchronizedCollection(new HashSet<RestClientRootUrl>());

    public WebServiceModule(VSTApplication app, Bus bus) {
        this.app = app;
        this.prefs = new AppPreferences_(app);
        this.bus = bus;
        bus.register(this);
    }

    ////   web service resource providers go below  /////

    @Singleton
    @Provides
    UserResource provideUserResource() {
        return init(new UserResource_(app));
    }

    @Singleton
    @Provides
    ConversationResource provideConversationResource() {
        return init(new ConversationResource_(app));
    }

    @Singleton
    @Provides
    SecureTextResource provideSecureTextResource() {
        return init(new SecureTextResource_(app));
    }

    ////  utility methods below //////

    private <T extends RestClientRootUrl> T  init(T resource) {
        resource.setRootUrl(prefs.restRootURL().get());
        clients.add(resource);
        return resource;
    }

    @Subscribe
    public void onBaseUrlChanged(RestRootURLPreferenceEvent event) {
        for (final RestClientRootUrl client : clients) {
            client.setRootUrl(event.getNewValue());
        }
    }

}
