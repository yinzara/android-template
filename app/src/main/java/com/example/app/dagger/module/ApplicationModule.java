package com.example.app.dagger.module;

import android.content.Context;
import android.view.LayoutInflater;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.squareup.otto.Bus;
import com.example.app.BuildConfig;
import com.example.app.VSTApplication;
import com.example.app.entity.dagger.module.DaoModule;
import com.example.app.state.AppPreferences_;
import com.example.app.state.DataState_;
import com.example.app.state.LoginState_;
import com.example.app.state.UserPreferences_;
import com.example.app.util.JacksonParceler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import flow.Flow;
import flow.StateParceler;

/**
 * General application scoped components
 *
 * Created by yinzara on 8/13/15.
 */
@Module(includes = {DatabaseModule.class, DaoModule.class, WebServiceModule.class})
public class ApplicationModule {

    private VSTApplication app;

    private Bus bus;

    public ApplicationModule(VSTApplication app, Bus bus) {
        this.app = app;
        this.bus = bus;
    }

//    @Singleton
//    @Provides
//    Context provideContext() {
//        return app;
//    }

    @Singleton
    @Provides
    ObjectMapper provideObjectMapper() {
        final ObjectMapper om = new ObjectMapper();

        om.registerModule(new JodaModule());

        om.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);

        om.enable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
        om.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        om.enable(DeserializationFeature.UNWRAP_SINGLE_VALUE_ARRAYS);
        om.enable(JsonParser.Feature.ALLOW_SINGLE_QUOTES);
        om.enable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES);

        om.disable(SerializationFeature.WRITE_NULL_MAP_VALUES);
        om.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
        om.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        if (BuildConfig.DEBUG) {
            om.setDefaultPrettyPrinter(new DefaultPrettyPrinter());
            om.enable(SerializationFeature.INDENT_OUTPUT);
        } else {
            om.setDefaultPrettyPrinter(null);
        }
        return om;
    }

    @Singleton
    @Provides
    Bus provideBus() {
        return bus;
    }

    @Provides
    StateParceler provideParceler(ObjectMapper objectMapper) {
        return new JacksonParceler(objectMapper);
    }

    @Singleton
    @Provides
    RefWatcher provideRefWatcher() {
        return LeakCanary.install(app);
    }

    @Provides
    LayoutInflater provideLayoutInflater() {
        return LayoutInflater.from(app);
    }

    @Provides
    UserPreferences_ providePrefs() {
        return new UserPreferences_(app);
    }

    @Provides
    LoginState_ provideLoginState() { return new LoginState_(app); }

    @Provides
    AppPreferences_ provideAppPreferences() { return new AppPreferences_(app); }

    @Provides
    DataState_ providesDataState() { return new DataState_(app); }

    @Provides
    Flow provideFlow() {
        return Flow.get(app);
    }
}
