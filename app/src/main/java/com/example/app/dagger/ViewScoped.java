package com.example.app.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by yinzara on 8/13/15.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ViewScoped {
}
