package com.example.app.dagger.component;

import com.example.app.MainActivity;
import com.example.app.VSTApplication;
import com.example.app.dagger.module.ApplicationModule;
import com.example.app.dagger.module.ViewModule;
import com.example.app.rest.base.JacksonMessageConverter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * This interface manages Dagger 2 dependency injection for all application scoped classes (such as MainActivity and VSTApplication)
 * <p></p>
 * If you would like to @Inject dependencies into a class:<br/>
 * 1. Add an "inject" method below view the class you would like to inject into<br/>
 * 2. In the class you will need to call VSTApplication.injector(context).inject(this) inside the constructor of the class or during the lifecycle where "context" is any android Context object and this is the object being injected<br/>
 * <p/>
 * Note: For the base classes in com.example.app.view.base, if you extend the class, do not do step 2 above, instead override the "doInject" method and call "component.inject(this);". See ConversationListView for example
 * <p/>
 * Created by yinzara on 8/13/15.
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    ViewComponent view(ViewModule viewModule);

    void inject(VSTApplication app);

    void inject(MainActivity activity);

    void inject(JacksonMessageConverter messageConverter);

    MediatorComponent mediator();

    PresenterComponent presenter();

    ServiceComponent service();
}
