package com.example.app.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by yinzara on 8/20/15.
 */
public class CometDDisconnectBroadcastReceiver extends BroadcastReceiver {

    public static final String STOP_SERVICE_IF_IN_BACKGROUND_ACTION = CometDDisconnectBroadcastReceiver.class.getName() + ".STOP";


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(STOP_SERVICE_IF_IN_BACKGROUND_ACTION)) {
            context.stopService(new Intent(context, CometDService.class));
        }
    }
}
