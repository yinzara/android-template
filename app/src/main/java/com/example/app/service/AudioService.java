package com.example.app.service;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.RawRes;
import android.util.Log;

import com.example.app.dagger.component.ServiceComponent;
import com.example.app.service.base.BaseService;

import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;
import org.androidannotations.annotations.SystemService;

import java.io.IOException;

/**
 * Created by yinzara on 8/17/15.
 */
@EIntentService
public class AudioService extends BaseService implements MediaPlayer.OnErrorListener, AudioManager.OnAudioFocusChangeListener {

    private static final String TAG = "AudioService";

    @SystemService
    AudioManager audioManager;

    private MediaPlayer audioPlayer;

    private boolean waitingForFocus = false;

    public AudioService() {
        super("AudioService");

    }

    @ServiceAction
    public void playAudioResource(@RawRes int sound) {
        final MediaPlayer mediaPlayer = MediaPlayer.create(this.getApplicationContext(), sound);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        final AudioManager.OnAudioFocusChangeListener listener = (int focusChange) -> {
            if (focusChange == AudioManager.AUDIOFOCUS_GAIN || focusChange == AudioManager.AUDIOFOCUS_GAIN_TRANSIENT || focusChange == AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK) {
                mediaPlayer.start();
            }
            //Don't do anything because we'll only be playing the sound for a very short time
        };
        int result = audioManager.requestAudioFocus(listener, AudioManager.STREAM_ALARM, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);

        mediaPlayer.setOnCompletionListener((final MediaPlayer mp) -> {
            mp.release();
            audioManager.abandonAudioFocus(listener);
        });
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mediaPlayer.start();
        }
    }

    @ServiceAction
    public void playAudio(Uri contentUri) {
        if (audioPlayer.isPlaying()) {
            audioPlayer.stop();
        }
        try {
            audioPlayer.setDataSource(getApplicationContext(), contentUri);
            audioPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,AudioManager.AUDIOFOCUS_GAIN);
            if (result != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                waitingForFocus = true;
            }
            audioPlayer.prepare();
            waitingForFocus = false;

        } catch (IOException exp) {
            Log.e(TAG, "IOException while playing streaming music with URI: " + contentUri, exp);
        }
    }

    @ServiceAction
    public void pauseAudio() {
        if (audioPlayer != null && audioPlayer.isPlaying()) {
            try {
                audioPlayer.pause();
            } catch (Exception exp) {
                Log.e(TAG, "Exception while pausing audio player", exp);
            }
        }
    }

    @ServiceAction
    public void resumeAudio() {
        if (audioPlayer != null && !audioPlayer.isPlaying()) {
            try {
                audioPlayer.start();
            } catch (Exception exp) {
                Log.e(TAG, "Exception while resuming audio player", exp);
            }
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        waitingForFocus = false;
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                if (!audioPlayer.isPlaying()) audioPlayer.start();
                audioPlayer.setVolume(1.0f, 1.0f);
                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback
                if (audioPlayer.isPlaying()) audioPlayer.stop();
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (audioPlayer.isPlaying()) audioPlayer.pause();
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (audioPlayer.isPlaying()) audioPlayer.setVolume(0.1f, 0.1f);
                break;
        }
    }

    @Override
    protected void doInject(ServiceComponent injector) {
        injector.inject(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        audioPlayer = new MediaPlayer();
        audioPlayer.setAudioStreamType(AudioManager.STREAM_SYSTEM);
        audioPlayer.setOnCompletionListener((final MediaPlayer mp) -> {
            audioManager.abandonAudioFocus(this);
        });
        audioPlayer.setOnPreparedListener((final MediaPlayer mp) -> {
            if (!AudioService.this.waitingForFocus) {
                mp.start();
            }
        });
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e(TAG, "MediaPlayer experiened an error: what=" + what + " & extra=" + extra);
        mp.reset();
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        audioManager.abandonAudioFocus(this);

        audioPlayer.release();
        audioPlayer = null;
    }
}
