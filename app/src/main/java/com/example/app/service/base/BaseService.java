package com.example.app.service.base;

import android.os.Parcelable;
import android.util.Log;

import com.squareup.otto.Bus;
import com.example.app.VSTApplication;
import com.example.app.dagger.component.ServiceComponent;
import com.example.app.mediator.ConnectivityMediator;
import com.example.app.mediator.GlobalEventMediator;
import com.example.app.dao.DaoSession;
import de.greenrobot.dao.HasId;
import com.example.app.event.EntityEvent;
import com.example.app.event.RemoteEvent;
import com.example.app.event.global.entity.base.BaseEntityEvent;
import com.example.app.mediator.StateMediator;
import com.example.app.rest.dto.BaseResponse;
import com.example.app.state.AppPreferences_;
import com.example.app.state.DataState_;
import com.example.app.state.LoginState_;
import com.example.app.state.UserPreferences_;

import net.sqlcipher.Cursor;
import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.api.support.app.AbstractIntentService;
import org.springframework.web.client.RestClientException;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import static com.example.app.mediator.base.BaseMediator.handleException;

/**
 * Classes that extend from this class are designed to handle long running background tasks that involve network and database operations<br/>
 * To create a new service:
 * <ol>
 *     <li>Extend from BaseService</li>
 *     <li>Add the @EIntentService annotation</li>
 *     <li>Override the doInject method and call "injector.inject(this);"</li>
 *     <li>Edit the ServiceComponent interface and add an "inject(NewClassService service);" method</li>
 *     <li>Edit the AndroidManifest.xml and add a new service entry (an example is shown in the file)</li>
 * </ol>
 *
 * Created by yinzara on 8/16/15.
 */
@EIntentService
public abstract class BaseService extends AbstractIntentService {

    private static final String TAG = "BS";

    public static final int DEFAULT_PAGE_SIZE = 30;

    @Bean
    protected GlobalEventMediator eventController;

    @Bean
    protected StateMediator stateMediator;

    @Bean
    protected ConnectivityMediator connectivity;

    @Inject
    protected DaoSession session;

    @Inject
    Bus bus;

    @AfterInject
    protected final void afterInject() {
        doInject(VSTApplication.injector(this).service());
    }

    protected abstract void doInject(ServiceComponent injector);

    protected UserPreferences_ userPrefs() {
        return stateMediator.getPrefs();
    }

    protected LoginState_ loginState() {
        return stateMediator.getLoginState();
    }

    protected DataState_ dataState() {
        return stateMediator.getDataState();
    }

    protected AppPreferences_ appPrefs() {
        return stateMediator.getAppPrefs();
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public BaseService(String name) {
        super(name);
    }

    @UiThread
    public void publish(final Object event) {
        bus.post(event);
    }

    public interface RemoteOperation<T> {
        void doRemote(T event) throws RestClientException;
    }

    public interface RemoteReturnOperation<T, J> {
        T doRemote(J event) throws RestClientException;
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable> void doRemote(EventType event, RemoteOperation<EventType> op) {
        doRemote(event, true, op);
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable> void doRemote(EventType event, Runnable op) {
        doRemote(event, true, op);
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable, J> J doRemote(EventType event, RemoteReturnOperation<J, EventType> op) {
        return doRemote(event, true, op);
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable, J> J doRemote(EventType event, Callable<J> op) {
        return doRemote(event, true, op);
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable> void doRemote(EventType event, boolean publish, RemoteOperation<EventType> op) {
        try {
            op.doRemote(event);
        } catch (RestClientException exp) {
            Log.e(TAG, "Exception during web service call", exp);
            event.setFailureReason(EntityEvent.FailureReason.REMOTE_ERROR);
        }
        if (publish) {
            publish(event);
        }
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable> void doRemote(EventType event, boolean publish, Runnable op) {
        try {
            op.run();
        } catch (RestClientException exp) {
            Log.e(TAG, "Exception during web service call", exp);
            event.setFailureReason(EntityEvent.FailureReason.REMOTE_ERROR);
        }
        if (publish) {
            publish(event);
        }
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable, J> J doRemote(EventType event, boolean publish, RemoteReturnOperation<J, EventType> op) {
        J val = null;
        try {
            val = op.doRemote(event);
        } catch (RestClientException exp) {
            Log.e(TAG, "Exception during web service call", exp);
            event.setFailureReason(EntityEvent.FailureReason.REMOTE_ERROR);
        }
        if (publish) {
            publish(event);
        }
        return val;
    }

    protected <EventType extends BaseEntityEvent<EntityType>, EntityType extends HasId & Parcelable, J> J doRemote(EventType event, boolean publish, Callable<J> op) {
        J val = null;
        try {
            val = op.call();
        } catch (Exception exp) {
            Log.e(TAG, "Exception during web service call", exp);
            event.setFailureReason(EntityEvent.FailureReason.REMOTE_ERROR);
        }
        if (publish) {
            publish(event);
        }
        return val;
    }

    protected <T extends RemoteEvent> void doRemote(T event, RemoteOperation<T> op) {
        try {
            op.doRemote(event);
        } catch (RestClientException exp) {
            handleException(event, exp);
        }
        publish(event);
    }

    protected <T extends RemoteEvent> void doRemote(T event, Runnable op) {
        try {
            op.run();
        } catch (RestClientException exp) {
            handleException(event, exp);
        }
        publish(event);
    }

    protected <T, J extends RemoteEvent> T doRemote(J event, RemoteReturnOperation<T, J> op) {
        return doRemote(event, true, op);
    }

    protected <T, J extends RemoteEvent> T doRemote(J event, boolean logNonSuccess, RemoteReturnOperation<T, J> op) {
        T val = null;
        try {
            val = op.doRemote(event);
            if (logNonSuccess) {
                if (val instanceof BaseResponse) {
                    final BaseResponse resp = (BaseResponse) val;
                    if (!resp.isSuccess()) {
                        Log.i(TAG, "Web service call failed: " + resp.getError());
                    }
                }
            }
        } catch (Exception exp) {
            handleException(event, exp);
        }
        publish(event);
        return val;
    }

    protected <T, J extends RemoteEvent> T doRemote(J event, Callable<T> op) {
        return doRemote(event, true, op);
    }

    protected <T, J extends RemoteEvent> T doRemote(J event, boolean logNonSuccess, Callable<T> op) {
        T val = null;
        try {
            val = op.call();
            if (logNonSuccess) {
                if (val instanceof BaseResponse) {
                    final BaseResponse resp = (BaseResponse) val;
                    if (!resp.isSuccess()) {
                        Log.i(TAG, "Web service call failed: " + resp.getError());
                    }
                }
            }
        } catch (Exception exp) {
            handleException(event, exp);
        }
        publish(event);
        return val;
    }

    protected void doInTransaction(Runnable op) {
        SQLiteDatabase db = session.getDatabase();
        db.beginTransaction();
        try {
            op.run();
            db.setTransactionSuccessful();
        } catch (Exception ex) {
            Log.e(TAG, "Exception during transaction", ex);
        } finally {
            db.endTransaction();
        }
    }

    protected <T> T doInTransaction(Callable<T> op) {
        SQLiteDatabase db = session.getDatabase();
        db.beginTransaction();
        T val = null;
        try {
            val = op.call();
            db.setTransactionSuccessful();
        } catch (Exception ex) {
            Log.e(TAG, "Exception during transaction", ex);
        } finally {
            db.endTransaction();
        }
        return val;
    }

    protected void executeRawSql(final String sql) {
        session.getDatabase().execSQL(sql);
    }

    protected void executeRawSql(final String sql, final Object... args) {
        session.getDatabase().execSQL(sql, args);
    }

    /**
     * @return the number of rows affected by the last update or delete
     */
    protected long getUpdatedCount() {
        Cursor cursor = null;
        try {
            cursor = session.getDatabase().rawQuery("SELECT changes() AS affected_row_count", null);
            if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
                return cursor.getLong(cursor.getColumnIndex("affected_row_count"));
            } else {
                // Some error occurred?
            }
        } catch (SQLException e) {
            // Handle exception here.
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return 0;
    }
}
