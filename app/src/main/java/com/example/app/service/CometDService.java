package com.example.app.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.v4.util.ArrayMap;
import android.util.Log;

import com.codeware.dto.securetext.SecureTextDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.example.app.VSTApplication;
import com.example.app.event.global.preference.RestRootURLPreferenceEvent;
import com.example.app.mediator.StateMediator;
import com.example.app.rest.dto.CometDMessageData;
import com.example.app.state.LoginState_;
import com.example.app.util.JacksonJSONContext;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.api.BackgroundExecutor;
import org.cometd.bayeux.Channel;
import org.cometd.bayeux.Message;
import org.cometd.bayeux.client.ClientSessionChannel;
import org.cometd.client.BayeuxClient;
import org.cometd.client.transport.ClientTransport;
import org.cometd.client.transport.LongPollingTransport;
import org.cometd.websocket.client.WebSocketTransport;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.websocket.WebSocketClient;
import org.eclipse.jetty.websocket.WebSocketClientFactory;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

import javax.inject.Inject;

/**
 * Created by yinzara on 8/20/15.
 */
@EService
public class CometDService extends Service {

    private static final String TAG = CometDService.class.getSimpleName();

    @Inject
    ObjectMapper objectMapper;

    @Inject
    Bus bus;

    @Bean
    StateMediator stateMediator;

    @SystemService
    ConnectivityManager cm;

    @AfterInject
    void doInject() {
        VSTApplication.injector(this).service().inject(this);
    }

    private BayeuxClient bayeuxClient;

    private BroadcastReceiver connectivityChangeReceiver;

    private WebSocketClientFactory wscf;

    private HttpClient httpClient;

    private String bayeuxUrl;

    private static class ConnectivityChangeReceiver extends BroadcastReceiver {

        private WeakReference<CometDService> local;

        private ConnectivityChangeReceiver(CometDService service) {
            this.local = new WeakReference<CometDService>(service);
        }


        @Override
        public void onReceive(Context context, Intent intent) {
            final CometDService service = local.get();
            if (service != null && service.bayeuxClient != null && !intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
                service.connectBayuexClientIfAccessible();
            }
        }
    }

    @Subscribe
    public void onUrlChanged(RestRootURLPreferenceEvent event) {
        if (!bayeuxUrl.equals(getBayeuxUrl())) {
            Log.v(TAG, "Resetting bayeux client from change in server URL");
            final boolean connected = isConnected();
            destroyBayeuxClient();
            createBayeuxClient();
            if (connected) {
                connectBayeuxClient();
            }
        }
    }

    private static class HandshakeListener implements ClientSessionChannel.MessageListener {

        private WeakReference<CometDService> local;

        public HandshakeListener(CometDService service) {
            this.local = new WeakReference<CometDService>(service);
        }

        @Override
        public void onMessage(ClientSessionChannel channel, Message message) {
            if (message.isSuccessful()) {
                if (message.getChannel().equals(Channel.META_HANDSHAKE)) {
                    Log.v(TAG, "Handshake message was successful");
                    final CometDService service = local.get();

                    if (service != null && service.bayeuxClient != null) {
                        final BayeuxClient client = service.bayeuxClient;

                        client.batch(new Runnable() {
                            @Override
                            public void run() {
                                final ClientSessionChannel textChannel = client.getChannel("/secure-text/text");

                                if (textChannel.getSubscribers() == null || textChannel.getSubscribers().isEmpty()) {
                                    textChannel.subscribe(new DocBeatMessageListener(service));
                                }
//                                final ClientSessionChannel voicemailChannel = client.getChannel("/secure-text/voicemail");
//                                if (voicemailChannel.getSubscribers() == null || voicemailChannel.getSubscribers().isEmpty()) {
//                                    voicemailChannel.subscribe(new VoicemailMessageListener(service));
//                                }
                            }
                        });
                    }
                } else {
                    Log.v(TAG, "Received message on HandshakeListener that was not a /meta/handshake: " + message.getJSON());
                }
            }
        }
    }

    private static class DocBeatMessageListener implements ClientSessionChannel.MessageListener {

        protected WeakReference<CometDService> local;

        public DocBeatMessageListener(CometDService service) {
            this.local = new WeakReference<CometDService>(service);
        }

        @Override
        public void onMessage(ClientSessionChannel channel, Message message) {
            if (message.getData() != null) {
                final CometDService service = local.get();
                final CometDMessageData data = (CometDMessageData)message.getData();
                if (data.getText() != null && data.getAction() != null) {
                    final SecureTextDTO text = data.getText();
                    final SecureTextDTO.SecureTextAction action = data.getAction().getAction();
                    SecureTextService_.intent(service).processSecureText(text, action).start();
                }
            }
        }
    }

    private static class CometDBayeuxClient extends BayeuxClient {

        public CometDBayeuxClient(final String url, final ScheduledExecutorService scheduler, final ClientTransport transport, final ClientTransport... transports) {
            super(url, scheduler, transport, transports);
        }

        @Override
        protected void failMessage(Message message, Throwable x) {
            super.failMessage(message, x);
            Log.e(TAG, "CometD failure with message: " + message.getJSON(), x);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createBayeuxClient() {
        Log.v(TAG, "Begin createBayeuxClient()");
        if (bayeuxClient == null) {

            final Map<String, Object> transportOptions = new ArrayMap<>(1);
            transportOptions.put(ClientTransport.JSON_CONTEXT, new JacksonJSONContext(objectMapper));


            //WebSocket transport setup
            wscf = new WebSocketClientFactory();
            wscf.getSslContextFactory().setProtocol("TLSv1");

            try {
                wscf.start();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            //LongPollingTransport setup
            httpClient = new HttpClient(wscf.getSslContextFactory());
            try {
                httpClient.start();
            } catch (Exception x) {
                throw new RuntimeException(x);
            }

            bayeuxUrl = getBayeuxUrl();
            final LongPollingTransport longPollingTransport = new LongPollingTransport(transportOptions, httpClient);

            final WebSocketClient webSocketClient = wscf.newWebSocketClient();
            final WebSocketTransport webSocketTransport = new WebSocketTransport(transportOptions, wscf, null);

            bayeuxClient = new CometDBayeuxClient(bayeuxUrl, null, webSocketTransport, longPollingTransport);

            bayeuxClient.setDebugEnabled(true);
//            webAppInterface = new DocBeatWebAppInterface(this);
        }
        Log.v(TAG, "End createBayeuxClient()");
    }

    protected String getBayeuxUrl() {
        return stateMediator.getAppPrefs().restRootURL().get() + "/cometd";
    }

    private void destroyBayeuxClient() {
        Log.v(TAG, "Begin destroyBayeuxClient()");
        if (bayeuxClient != null) {

            bayeuxClient.abort();
            bayeuxClient = null;

            try {
                wscf.stop();
            } catch (Exception exp) {
            }
            wscf = null;

            try {
                httpClient.stop();
            } catch (Exception exp) {
            }
            httpClient = null;
        }
        Log.v(TAG, "End destroyBayeuxClient()");
    }

    private boolean isConnected() {
        return bayeuxClient != null ? bayeuxClient.isConnected() : false;
    }

    private boolean connectBayuexClientIfAccessible() {
        if (isNetworkAvailable() && bayeuxClient != null && bayeuxClient.isDisconnected()) {
            Log.v(TAG, "Connecting BayeuxClient as network is available");
            return connectBayeuxClient();
        } else {
            Log.v(TAG, "Not connecting BayeuxClient as network is unavailable or bayeuxClient in bad state");
            return false;
        }
    }

    private boolean isNetworkAvailable() {
        final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    private boolean connectBayeuxClient() {
        Log.v(TAG, "Begin connectBayeuxClient()");
        try {
            if (bayeuxClient != null && bayeuxClient.isDisconnected()) {
                final LoginState_ loginState = stateMediator.getLoginState();
                final String token = loginState.token().get();
                if (token == null || token.isEmpty()) {
                    return false;
                }
                final int staffId = loginState.staffId().getOr(0);
                final Map<String, Object> template = new ArrayMap<>();
                final Map<String, Object> data = new ArrayMap<>();
                template.put("data", data);
                data.put("staffId", staffId);
                data.put("token", token);
                bayeuxClient.handshake(template, new HandshakeListener(this));
                Log.v(TAG, "BayeuxClient handshake sent for URL: " + bayeuxUrl);
                return true;
            }

            return false;
        } finally {
            Log.v(TAG, "End connectBayeuxClient()");
        }
    }

    private void disconnectBayeuxClient() {
        Log.v(TAG, "Begin disconnectBayeuxClient()");
        if (bayeuxClient != null) {
            bayeuxClient.disconnect();
        }
        Log.v(TAG, "End disconnectBayeuxClient()");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "Begin onCreate()");

        connectivityChangeReceiver = new ConnectivityChangeReceiver(this);
        registerReceiver(connectivityChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


        BackgroundExecutor.execute(() -> {
            try {
                createBayeuxClient();
                bus.register(this);
                connectBayuexClientIfAccessible();
            } catch (RuntimeException exp) {
                Log.e(TAG, "Exception while initializing Bayeux", exp);
            }
        });


        Log.v(TAG, "End onCreate()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Begin onDestroy()");

        BackgroundExecutor.execute(() -> {
            try {
                destroyBayeuxClient();
            } catch (RuntimeException exp) {
                Log.e(TAG, "Exception while destroying Bayeux", exp);
            }
        });

        unregisterReceiver(connectivityChangeReceiver);
        connectivityChangeReceiver = null;

        bus.unregister(this);

        Log.d(TAG, "End onDestroy()");
    }
}
