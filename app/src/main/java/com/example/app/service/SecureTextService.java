package com.example.app.service;

import android.util.Log;

import com.codeware.dto.PageResponse;
import com.codeware.dto.securetext.AttachmentDTO;
import com.codeware.dto.securetext.SecureTextDTO;
import com.codeware.dto.securetext.SecureTextListResultDTO;
import com.example.app.BuildConfig;
import com.example.app.dagger.component.ServiceComponent;
import com.example.app.dao.AttachmentDao;
import com.example.app.dao.ConversationDao;
import com.example.app.dao.SecureTextDao;
import com.example.app.dao.StaffDao;
import com.example.app.entity.Attachment;
import com.example.app.entity.Conversation;
import com.example.app.entity.SecureText;
import com.example.app.entity.Staff;
import com.example.app.event.EntityEvent;
import com.example.app.event.global.entity.ConversationEvent;
import com.example.app.event.global.entity.ConversationListEvent;
import com.example.app.event.global.entity.SecureTextEvent;
import com.example.app.event.global.entity.SecureTextListEvent;
import com.example.app.mediator.LoginMediator;
import com.example.app.rest.SecureTextResource;
import com.example.app.service.base.BaseService;
import com.example.app.util.Utils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;
import org.joda.time.DateTime;
import org.springframework.web.client.RestClientException;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import de.greenrobot.dao.query.QueryBuilder;

import static com.example.app.util.Utils.newer;
import static com.example.app.util.Utils.older;

/**
 * Created by yinzara on 8/16/15.
 */
@EIntentService
public class SecureTextService extends BaseService {

    private static final String TAG = "SecTxtSvc";

    private static final String DELETE_EMPTY_CONVERSATIONS = "DELETE FROM " + ConversationDao.TABLENAME +
            " WHERE _id IN (SELECT _id FROM " + ConversationDao.TABLENAME + " WHERE NOT EXISTS" +
            " (SELECT _id FROM " + SecureTextDao.TABLENAME + " WHERE " +
            SecureTextDao.Properties.ThreadId.columnName + " = " + ConversationDao.TABLENAME + "._id))";

    private static final String DELETE_CONVERSATION_IF_EMPTY = "DELETE FROM " + ConversationDao.TABLENAME +
            " WHERE _id = ? AND NOT EXISTS" +
            " (SELECT _id FROM " + SecureTextDao.TABLENAME + " WHERE " +
            SecureTextDao.Properties.ThreadId.columnName + " = " + ConversationDao.TABLENAME + "._id)";

    private static final String MARK_DELETED_CONVERSATIONS = "UPDATE " + ConversationDao.TABLENAME +
            " SET DELETED = 1" +
            " WHERE _id IN (SELECT _id FROM " + ConversationDao.TABLENAME + " WHERE NOT EXISTS" +
            " (SELECT _id FROM " + SecureTextDao.TABLENAME + " WHERE " + SecureTextDao.TABLENAME + ".DELETED = 0 AND " +
            SecureTextDao.Properties.ThreadId.columnName + " = " + ConversationDao.TABLENAME + "._id))";

    private static final String MARK_DELETED_CONVERSATION_IF_EMPTY = "UPDATE " + ConversationDao.TABLENAME +
            " SET DELETED = 1" +
            " WHERE _id = ? AND NOT EXISTS" +
            " (SELECT _id FROM " + SecureTextDao.TABLENAME + " WHERE " + SecureTextDao.TABLENAME + ".DELETED = 0 AND " +
            SecureTextDao.Properties.ThreadId.columnName + " = " + ConversationDao.TABLENAME + "._id)";

    private static final String DELETE_EXPIRED_MESSAGES = "DELETE FROM " + SecureTextDao.TABLENAME + " WHERE expired < ?";

    private static String deleteEmptyConversationsSql(final Collection<Long> ids) {
        final StringBuilder buf = new StringBuilder();
        buf.append("DELETE FROM ")
                .append(ConversationDao.TABLENAME)
                .append(" WHERE _id IN (");
        boolean isFirst = false;
        for (Long id : ids) {
            if (!isFirst) {
                buf.append(',');
            }
            buf.append(id);
        }
        return buf.append(") AND NOT EXISTS")
                .append(" (SELECT _id FROM ").append(SecureTextDao.TABLENAME).append(" WHERE ")
                .append(SecureTextDao.Properties.ThreadId.columnName).append(" = ").append(ConversationDao.TABLENAME).append("._id)").toString();
    }

    private static String markDeletedConversationsIfEmptySql(final Collection<Long> ids) {
        final StringBuilder buf = new StringBuilder();
        buf.append("UPDATE ")
                .append(ConversationDao.TABLENAME)
                .append(" SET DELETED = 1")
                .append(" WHERE _id IN (");
        boolean isFirst = false;
        for (Long id : ids) {
            if (!isFirst) {
                buf.append(',');
            }
            buf.append(id);
        }
        return buf.append(") AND NOT EXISTS")
                .append(" (SELECT _id FROM ").append(SecureTextDao.TABLENAME).append(" WHERE ")
                .append(SecureTextDao.TABLENAME).append(".DELETED = 0 AND ")
                .append(SecureTextDao.Properties.ThreadId.columnName).append(" = ").append(ConversationDao.TABLENAME).append("._id)").toString();
    }

    @Inject
    SecureTextResource secureTextResource;

    @Inject
    SecureTextDao secureTextDao;

    @Inject
    ConversationDao conversationDao;

    @Inject
    AttachmentDao attachmentDao;

    @Inject
    StaffDao staffDao;

    @Bean
    LoginMediator loginMediator;

    private boolean loadingRecentSecureTexts;

    public SecureTextService() {
        super("SecureTextService");
    }

    @ServiceAction
    public void postSecureTextRead(final long id) {
        if (connectivity.isConnectedToNetwork()) {
            doInTransaction(() -> {
                final SecureText text = secureTextDao.load(id);
                final String staffIdStr = loginState().staffId().get().toString();
                if (text.getRead() == null && text.getTo().equals(staffIdStr)) {
                    secureTextResource.postSecureTextsRead(id);
                }
            });
        }
    }

    @ServiceAction
    public void postConversationRead(final long id) {
        if (connectivity.isConnectedToNetwork()) {
            final Set<Long> idsToMarkRead = new HashSet<Long>();
            final String staffIdStr = loginState().staffId().get().toString();

            final QueryBuilder<SecureText> builder = secureTextDao.queryBuilder();
            builder.where(
                    SecureTextDao.Properties.ThreadId.eq(id),
                    SecureTextDao.Properties.Read.isNull(),
                    SecureTextDao.Properties.To.eq(staffIdStr)
            );

            idsToMarkRead.addAll(builder.buildProjection(SecureTextDao.Properties.SecureTextId).list());

            if (!idsToMarkRead.isEmpty()) {
                secureTextResource.postSecureTextsRead(Utils.toArray(idsToMarkRead));
            }
        }
    }

    @ServiceAction
    public void loadMostRecentSecureTexts() {
        if (!loginMediator.isLoggedIn()) {
            return;
        }
        if (!loadingRecentSecureTexts) {
            int firstResult = 0;
            int maxResults = DEFAULT_PAGE_SIZE;
            loadingRecentSecureTexts = true;
            try {
                loadMostRecentSecureTexts(firstResult, maxResults, getLastSecureTextSync(), new HashSet<Long>());
            } finally {
                loadingRecentSecureTexts = false;
            }
        }
    }

    private void loadMostRecentSecureTexts(int firstResult, int maxResults, final DateTime before, final Set<Long> convosToCheckForDelete) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Loading recent SecureText offset=" + firstResult + " limit=" + maxResults + " before=" + before);
        }
        final SecureTextListEvent event = new SecureTextListEvent();
        final ConversationListEvent convoEvent = new ConversationListEvent();
        final Set<Long> idsToDeliver = new HashSet<>(50);
        final String staffIdStr = loginState().staffId().get().toString();

        final SecureTextListResultDTO resp = doInTransaction(() -> {
            final Date now = new Date();
            final SecureTextListResultDTO result = secureTextResource.getMostRecentSecureTexts(before, firstResult, maxResults);
            if (result.getTexts() != null && result.getTexts().getItems() != null) {
                final PageResponse<SecureTextDTO> texts = result.getTexts();
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Secure texts loaded: qty=" + (texts.getItems() != null ? texts.getItems().size() : "0") + "  total=" + texts.getTotalItems());
                }
                final Set<Conversation> dirtyConversations = new HashSet<Conversation>(texts.getItems().size());
                final Map<Long, Conversation> conversationMap = new HashMap<>();
                int newConvos = 0;

                for (final SecureTextDTO text : texts.getItems()) {
                    if (text.getDeleted() != null || (text.getExpiration() != null && text.getExpiration().before(now))) {
                        //this means the secure text has been deleted or has expired, remove it from the DB
                        secureTextDao.deleteByKey(text.getSecureTextOwnerId());
                        convosToCheckForDelete.add(text.getThreadID());
                        event.getDeleted().add(text.getSecureTextOwnerId());
                        continue;
                    }
                    Conversation convo = conversationMap.get(text.getThreadID());
                    if (convo == null) {
                        convo = conversationDao.load(text.getThreadID());
                        if (convo == null) {
                            convo = new Conversation();
                            convo.setId(text.getThreadID());
                            conversationDao.insert(convo);
                            newConvos++;
                            convoEvent.getInserted().add(text.getThreadID());
                        }
                        conversationMap.put(text.getThreadID(), convo);
                    }

                    final SecureText entity = processNewSecureText(text, convo, event);
                    if (entity.getDelivered() == null && entity.getTo().equals(staffIdStr)) {
                        //The post action method takes a secureTextId, NOT a secureTextOwnerId
                        idsToDeliver.add(entity.getSecureTextId());
                    }

                    if (convo.getLastActivity() == null || convo.getLastActivity().before(entity.getUpdated())) {
                        convo.setLastMessage(entity);
                        convo.setLastActivity(new Date());
                        dirtyConversations.add(convo);
                    }
                }

                for (final Conversation convo : dirtyConversations) {
                    convo.update();
                    convoEvent.getUpdated().add(convo);
                }
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, dirtyConversations.size() + " conversations with new messages, " + newConvos + " new conversations");
                }
            }
            return result;
        });

        publish(event);
        publish(convoEvent);

        try {
            secureTextResource.postSecureTextsDelivered(Utils.toArray(idsToDeliver));
        } catch (RestClientException exp) {
            Log.e(TAG, "Exception while posting delivery of messages", exp);
        }

        if (resp != null && resp.getTexts() != null && resp.getTexts().getItems() != null && resp.getTexts().getItems().size() == maxResults) {
            //We have more paging to do
            loadMostRecentSecureTexts(firstResult + maxResults, maxResults, before, convosToCheckForDelete);
        } else {
            //paging complete and we're done
            updateLastSyncDate();
            deleteConversationsIfEmpty(convosToCheckForDelete);
        }
    }

    private SecureText processNewSecureText(final SecureTextDTO dto, final Conversation conversation) {
        return processNewSecureText(dto, conversation, null);
    }

    private SecureText processNewSecureText(final SecureTextDTO text, final Conversation convo, final SecureTextListEvent event) {
        SecureText entity = event != null ? secureTextDao.load(text.getSecureTextOwnerId()) : null;
        boolean newEntity = false;
        if (entity == null) {
            entity = new SecureText();
            newEntity = true;

            entity.setConversation(convo);

            entity.setId(text.getSecureTextOwnerId());
            entity.setSecureTextId(text.getSecureTextId());
            entity.setUid(text.getUniqueId());
            entity.setType(text.getType());

            if (text.getPriority() != null) {
                try {
                    entity.setPriority(Integer.valueOf(text.getPriority()));
                } catch (NumberFormatException exp) {
                    //Ignore as it would be an invalid value
                }
            }

            entity.setMessage(text.getMessage());

            entity.setTo(text.getTo());
            entity.setFrom(text.getFrom());

            try {
                final long toStaffId = Long.parseLong(entity.getTo());
                final Staff staff = staffDao.load(toStaffId);
                if (staff != null) {
                    entity.setToStaff(staff);
                }
            } catch (NumberFormatException exp) {
                //Ignore
            }
            try {
                final long fromStaffId = Long.parseLong(entity.getFrom());
                final Staff staff = staffDao.load(fromStaffId);
                if (staff != null) {
                    entity.setFromStaff(staff);
                }
            } catch (NumberFormatException exp) {
                //Ignore
            }

            entity.setOwner(text.getOwner());
            entity.setOtherParty(text.getOtherParty());

            if (text.getAttachments() != null && !text.getAttachments().isEmpty()) {
                if (newEntity) {
                    for (final AttachmentDTO attachment : text.getAttachments()) {
                        final Attachment att = new Attachment();
                        att.setId(attachment.getAttachmentId());
                        att.setFilename(attachment.getFilename());
                        att.setSecureTextOwnerId(entity.getId());
                        attachmentDao.insert(att);
                    }
                } else {
                    for (final Attachment existing : entity.getAttachments()) {
                        for (final AttachmentDTO dto : text.getAttachments()) {
                            if (dto.getAttachmentId() == existing.getId()) {
                                if (dto.getFilename() != null && !dto.getFilename().equals(existing.getFilename())) {
                                    existing.setFilename(dto.getFilename());
                                    existing.update();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        boolean changed = false;
        //Don't set 'updated=true' simply because the updated or created field changed as it has no value
        if (older(entity.getCreated(), text.getCreated())) {
            entity.setCreated(text.getCreated());
        }
        if (newer(entity.getUpdated(), text.getUpdated())) {
            entity.setUpdated(text.getUpdated());
        }
        if (entity.getCreated() == null) {
            entity.setCreated(new Date());
        }
        if (entity.getUpdated() == null) {
            entity.setUpdated(new Date());
        }

        if (changed |= older(entity.getSent(), text.getSent())) {
            entity.setSent(text.getSent());
        }
        if (changed |= older(entity.getScheduled(), text.getScheduled())) {
            entity.setScheduled(text.getScheduled());
        }
        if (changed |= older(entity.getNotified(), text.getNotified())) {
            entity.setNotified(text.getNotified());
        }
        if (changed |= older(entity.getDelivered(), text.getDelivered())) {
            entity.setDelivered(text.getDelivered());
        }
        if (changed |= older(entity.getRead(), text.getRead())) {
            entity.setRead(text.getRead());
        }
        if (changed |= older(entity.getRecalled(), text.getRecalled())) {
            entity.setRecalled(text.getRecalled());
        }
        if (changed |= newer(entity.getExpiration(), text.getExpiration())) {
            entity.setExpiration(text.getExpiration());
        }

        if (newEntity) {
            secureTextDao.insert(entity);
            if (event != null) {
                event.getInserted().add(entity.getId());
            }
        } else if (changed) {
            entity.update();
            if (event != null) {
                event.getUpdated().add(entity);
            }
        }
        return entity;
    }

    @ServiceAction
    public void deleteConversationIfEmpty(long conversationId) {
        doInTransaction(() -> {
            executeRawSql(DELETE_CONVERSATION_IF_EMPTY, conversationId);
            executeRawSql(MARK_DELETED_CONVERSATION_IF_EMPTY, conversationId);
        });
    }

    @ServiceAction
    public void deleteConversationsIfEmpty(long... conversationsId) {
        deleteConversationsIfEmpty(Utils.toList(conversationsId));
    }

    private void deleteConversationsIfEmpty(Long... conversationsId) {
        deleteConversationsIfEmpty(Arrays.asList(conversationsId));
    }

    private void deleteConversationsIfEmpty(Collection<Long> conversationIds) {
        doInTransaction(() -> {
            executeRawSql(deleteEmptyConversationsSql(conversationIds));
            executeRawSql(markDeletedConversationsIfEmptySql(conversationIds));
        });
    }

    @ServiceAction
    public void reapExpiredMessages() {
        doInTransaction(() -> {
            executeRawSql(DELETE_EXPIRED_MESSAGES, new Date());
            executeRawSql(DELETE_EMPTY_CONVERSATIONS);
            executeRawSql(MARK_DELETED_CONVERSATIONS);
        });
    }

    private DateTime getLastSecureTextSync() {
        final long lastSync = dataState().lastSecureTextSync().get();
        if (lastSync == 0) {
            return DateTime.now().minusMonths(2);
        } else {
            return new DateTime(lastSync);
        }
    }

    private void updateLastSyncDate() {
        if (loginState().staffId().getOr(0) > 0) {
            dataState().lastSecureTextSync().put(System.currentTimeMillis());
        }
    }

    @ServiceAction
    public void processSecureText(final SecureTextDTO dto, final SecureTextDTO.SecureTextAction action) {
        doInTransaction(() -> {
            SecureText text = secureTextDao.load(dto.getSecureTextOwnerId());
            boolean newConversation = false;
            Conversation conversation;
            if (text != null) {
                conversation = text.getConversation();
                boolean updated = false;
                switch (action) {
                    case DELETE:
                        secureTextDao.delete(text);
                        deleteConversationIfEmpty(text.getThreadId());
                        publish(new SecureTextEvent().setType(EntityEvent.Type.DELETE).setEntity(text));
                        break;
                    case DELIVERY:
                        text.setDelivered(new Date());
                        text.setUpdated(text.getDelivered());
                        updated = true;
                        break;
                    case NOTIFICATION:
                        text.setNotified(new Date());
                        text.setUpdated(text.getNotified());
                        updated = true;
                        break;
                    case READ:
                        text.setRead(new Date());
                        text.setUpdated(text.getRead());
                        updated = true;
                        break;
                    case UPDATE:
                        text.setUpdated(new Date());
                        updated = true;
                        break;
                }
                if (updated) {
                    text.update();
                    publish(new SecureTextEvent().setType(EntityEvent.Type.UPDATE).setEntity(text));
                }
            } else if (action != SecureTextDTO.SecureTextAction.DELETE) {
                conversation = conversationDao.load(dto.getThreadID());
                if (conversation == null) {
                    conversation = new Conversation();
                    conversation.setId(dto.getThreadID());
                    conversationDao.insert(conversation);
                    newConversation = true;
                }
                text = SecureTextService.this.processNewSecureText(dto, conversation);
                publish(new SecureTextEvent().setType(EntityEvent.Type.INSERT).setEntity(text));
            } else {
                conversation = null;
            }

            if (conversation != null && (conversation.getLastActivity() == null || conversation.getLastActivity().before(text.getUpdated()))) {
                conversation.setLastMessage(text);
                conversation.setLastActivity(new Date());
                conversation.update();
                publish(new ConversationEvent().setType(newConversation ? EntityEvent.Type.INSERT : EntityEvent.Type.UPDATE).setEntity(conversation));
            }
        });

        loadMostRecentSecureTexts();
    }

    @Override
    protected void doInject(ServiceComponent injector) {
        injector.inject(this);
    }
}
