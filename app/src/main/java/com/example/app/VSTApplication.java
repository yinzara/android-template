package com.example.app;

import android.app.Application;
import android.content.Context;
import android.view.View;

import com.squareup.leakcanary.RefWatcher;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import com.example.app.dagger.component.ApplicationComponent;
import com.example.app.dagger.component .DaggerApplicationComponent;
import com.example.app.dagger.component.ViewComponent;
import com.example.app.dagger.module.ApplicationModule;
import com.example.app.entity.dagger.module.DaoModule;
import com.example.app.dagger.module.DatabaseModule;
import com.example.app.dagger.module.ViewModule;
import com.example.app.dagger.module.WebServiceModule;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import org.androidannotations.annotations.EApplication;

import javax.inject.Inject;

import mortar.MortarScope;

/**
 * Created by yinzara on 8/12/15.
 */
@EApplication
public class VSTApplication extends Application {

    /**
     * Content provider authority for files in the /files dir
     */
    public static final String FILE_CONTENT_AUTHORITY = "com.example.app.file";

    private ApplicationComponent applicationComponent;

    @Inject
    RefWatcher refWatcher;

    @Inject
    SQLiteOpenHelper openHelper;

    @Inject
    SQLiteDatabase database;

    public static ApplicationComponent injector(final Context context) {
        return ((VSTApplication) context.getApplicationContext()).applicationComponent;
    }

    public static ViewComponent injector(final View view) {
        return injector(view.getContext()).view(new ViewModule(view));
    }

    public static RefWatcher refWatcher(final Context context) {
        return ((VSTApplication) context.getApplicationContext()).refWatcher;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        final Bus bus = new Bus(ThreadEnforcer.MAIN);
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this, bus))
                .databaseModule(new DatabaseModule(this))
                .daoModule(new DaoModule())
                .webServiceModule(new WebServiceModule(this, bus))
                .build();
        applicationComponent.inject(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        try {
            database.close();
        } catch (Exception exp) {
        }

        try {
            openHelper.close();
        } catch (Exception exp) {
        }
    }

    private MortarScope rootScope;

    @Override public Object getSystemService(String name) {
        if (rootScope == null) rootScope = MortarScope.buildRootScope().build("APP");

        return rootScope.hasService(name) ? rootScope.getService(name) : super.getSystemService(name);
    }
}
