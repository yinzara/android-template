package com.example.app.daogenerator;

import net.sqlcipher.database.SQLiteDatabase;

/**
 * Created by yinzara on 8/13/15.
 */
public class Upgrader {

    public static final int CURRENT_DB_VERSION = 3;

    /**
     * This method is called whenever an on device database needs to be upgraded
     *
     * Any changes to the entities should have a corresponding entry here to correct the schema
     *
     * @param sqLiteDatabase
     * @param oldVersion
     * @param newVersion
     */
    public static void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {

        }
    }
}
