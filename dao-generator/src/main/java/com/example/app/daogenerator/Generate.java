package com.example.app.daogenerator;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.io.File;
import java.util.Date;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Index;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.PropertyType;
import de.greenrobot.daogenerator.Schema;

/**
 * This class generates all of the database schema and entities/daos for GreenDAO
 * <p></p>
 * You will need to edit the DatabaseModule class in the app project for each new entity created here so the DAO can be injected appropriately
 */
public class Generate {

    private Schema schema;

    private Entity staff;

    private Entity group;

    private Entity convo;

    private Entity secureText;

    public Generate(final Schema schema) {
        this.schema = schema;
    }

    public static void main(String[] args) throws Exception {
        final Schema schema = new Schema(Upgrader.CURRENT_DB_VERSION, "com.example.app");
        schema.setDefaultJavaPackage("com.example.app.entity");
        schema.setDefaultJavaPackageDao("com.example.app.dao");

        final File srcGen = new File("../app/src-gen/main/java");

        if (!srcGen.exists()) {
            final File temp = new File(srcGen, "temp");
            temp.mkdirs();
            temp.createNewFile();
            temp.delete();
        }
        final Generate gen = new Generate(schema);



        gen.genStaffAndOrgs();

        gen.genTextTables();

        schema.enableKeepSectionsByDefault();
        schema.enableParcelableGenerated();
        schema.enableEventGenerated();
        schema.setDefaultJavaPackageEvent("com.example.app.event.global.entity");

        new DaoGenerator().generateAll(schema, "../app/src-gen/main/java", "../app/src/main/res");

        //Delete any files that are older (which generally means they shouldn't be there (i.e they weren't generated with this run)
        File newest = null;
        for (final File file : FileUtils.listFiles(srcGen, new String[] {"java"}, true)) {
            if (newest == null || FileUtils.isFileNewer(file, newest)) {
                newest = file;
            }
        }

        for (final File file : FileUtils.listFiles(srcGen, FileFilterUtils.ageFileFilter(newest.lastModified() - 5000, true), FileFilterUtils.directoryFileFilter())) {
            file.delete();
        }

        //Delete empty directories in 'srcGen' directory
        boolean foundAny;
        do {
            foundAny = false;

            for (final File dir : FileUtils.listFilesAndDirs(srcGen, FileFilterUtils.falseFileFilter(), TrueFileFilter.INSTANCE)) {
                if (dir.isDirectory() && dir.list().length == 0) {
                    dir.delete();
                    foundAny = true;
                }
            }

        } while (foundAny);
    }

    private void genStaffAndOrgs() {
        group = schema.addEntity("OrganizationalGroup");
//        group.implementsInterface(HAS_ID_CLASS);
        group.addIdProperty().notNull();
        group.addStringProperty("name");
        group.setGenerateListView(true);

        final Property parentId = group.addLongProperty("parentId").getProperty();
        group.addToOne(group, parentId, "parent");
        group.addToMany(group, parentId, "children");

        staff = schema.addEntity("Staff");
//        staff.implementsInterface(HAS_ID_CLASS);
        staff.addIdProperty().notNull();
        staff.addStringProperty("firstName");
        staff.addStringProperty("lastName");
        staff.addStringProperty("title");
        staff.setGenerateListView(true);

        final Entity staffOrgGroup = schema.addEntity("StaffOrganizationalGroup");
//        staffOrgGroup.implementsInterface(HAS_ID_CLASS);
        staffOrgGroup.addIdProperty().notNull();  // ID
        final Property SOGstaffId = staffOrgGroup.addLongProperty("staffId").notNull().index().getProperty();
        final Property SOGgroupId = staffOrgGroup.addLongProperty("groupId").notNull().index().getProperty();

        final Index sogIndex = new Index();
        sogIndex.addProperty(SOGgroupId);
        sogIndex.addProperty(SOGstaffId);
        sogIndex.makeUnique();
        staffOrgGroup.addIndex(sogIndex);

        staffOrgGroup.addToOne(staff, SOGstaffId, "staff");
        staffOrgGroup.addToOne(group, SOGgroupId, "group");

        staff.addToMany(staffOrgGroup, SOGstaffId, "groups");
        group.addToMany(staffOrgGroup, SOGgroupId, "staff");
    }

    private void genTextTables() {
        convo = schema.addEntity("Conversation");
//        convo.implementsInterface(HAS_ID_CLASS);
        convo.addIdProperty().notNull();
        convo.addBooleanProperty("deleted").notNull();
        convo.addDateProperty("lastActivity");
        convo.setGenerateListView(true);

//        convo.addBooleanProperty("testBool");
//        convo.addBooleanProperty("testBoolNotNull").notNull();
//        convo.addByteArrayProperty("testByteArray");
//        convo.addByteArrayProperty("testByteArrayNotNull").notNull();
//        convo.addDateProperty("testDateNotNull").notNull();
//        convo.addStringProperty("testString");
//        convo.addStringProperty("testStringNotNull").notNull();
//        convo.addIntProperty("testInt");
//        convo.addIntProperty("testIntNotNull").notNull();
//        convo.addDoubleProperty("testDouble");
//        convo.addDoubleProperty("testDoubleNotNull").notNull();
//        convo.addFloatProperty("testFloat");
//        convo.addFloatProperty("testFloatNotNull").notNull();


        final Property lastMessageId = convo.addLongProperty("lastMessageId").getProperty();

        secureText = schema.addEntity("SecureText");
        secureText.setGenerateListView(true);
//        secureText.implementsInterface(HAS_ID_CLASS);
        secureText.addIdProperty().notNull();
        secureText.addLongProperty("secureTextId").notNull();
        secureText.addStringProperty("uid").notNull();


        secureText.addStringProperty("message").notNull();

        secureText.addIntProperty("type").notNull();
        secureText.addStringProperty("to").notNull();
        secureText.addStringProperty("from").notNull();
        secureText.addStringProperty("owner").notNull();
        secureText.addStringProperty("otherParty").notNull();

        secureText.addIntProperty("priority");

        secureText.addDateProperty("sent").notNull().index();
        secureText.addDateProperty("scheduled");
        secureText.addDateProperty("notified");
        secureText.addDateProperty("delivered");
        secureText.addDateProperty("read");
        secureText.addDateProperty("recalled");
        secureText.addDateProperty("expiration");
        secureText.addDateProperty("created").notNull();
        secureText.addDateProperty("updated").notNull();

        secureText.addBooleanProperty("deleted").notNull();

        final Property toStaffId = secureText.addLongProperty("toStaffId").getProperty();
        final Property fromStaffId = secureText.addLongProperty("fromStaffId").getProperty();
        final Property conversationId = secureText.addLongProperty("threadId").notNull().getProperty();

        secureText.addToOne(staff, toStaffId, "toStaff");
        secureText.addToOne(staff, fromStaffId, "fromStaff");
        secureText.addToOne(convo, conversationId, "conversation");

        convo.addToOne(secureText, lastMessageId, "lastMessage");
        convo.addToMany(secureText, conversationId, "messages");

        final Entity attachment = schema.addEntity("Attachment");
        attachment.addIdProperty().notNull();
        attachment.addStringProperty("filename").notNull();
        attachment.setGenerateListView(true);
        final Property attachment_secureTextOwner = attachment.addLongProperty("secureTextOwnerId").notNull().getProperty();
        attachment.addToOne(secureText, attachment_secureTextOwner, "secureText");

        secureText.addToMany(attachment, attachment_secureTextOwner, "attachments").enableParceled();



        final Entity voicemail = schema.addEntity("Voicemail");
        voicemail.addIdProperty().notNull();
        voicemail.addLongProperty("soundClipId");
        voicemail.addStringProperty("from").notNull();
        voicemail.addStringProperty("message");
        voicemail.addIntProperty("duration").notNull();
        voicemail.setGenerateListView(true);
        voicemail.addToOne(staff, voicemail.addLongProperty("fromStaffId").getProperty(), "fromStaff");
    }


    private static void delete(final File file) {
        if (file.isDirectory()) {
            for (final File file2 : file.listFiles()) {
                delete(file2);
            }
        } else {
            file.delete();
        }
    }
}
